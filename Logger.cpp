#include <Arduino.h>
#include <DB.h>
#include "Enums.h"
#include "Eepromy.h"
#include "Logger.h"



// ################# Global Variables #################

const byte eventlogStartAddr = 100;
Record allEventlogRecords[10];// 0 - 10

// ################# Objects #################
DB db;// Event log DB



void initDb()
{
  // ### Enable below line to regenerate database. ###
  // Record eventlogRecord;
  //db.create(eventlogStartAddr,sizeof(eventlogRecord));
  db.open(eventlogStartAddr);
  delay(10);
}



void logEvent(int evnt, boolean force){// If forced, anti-repeat is disabled
  
  uint8_t pointer = eepromLoadLoggerPointer();

  
  // Check two previous entries to prevent repeated dupe logs
  if(!force && eventTriggeredToday(evnt))return;
  
  Record eventlogRecord;
  
  if(pointer<109){// Pointer 100 - 109
    pointer++;
  }else{
    pointer = 100;
  }
  
  byte record = pointer - 99;// 1-10
  
  time_t t = now();
 
  
  eventlogRecord.dYear = year(t);
  eventlogRecord.dMonth = month(t);
  eventlogRecord.dDay = day(t);
  eventlogRecord.tHour = hour(t);
  eventlogRecord.tMinute = minute(t);
  eventlogRecord.eType = evnt;
  eventlogRecord.tTime = t;
  
  //if(!db.insert(record, DB_REC eventlogRecord))db.append(DB_REC eventlogRecord);
  if(record <= db.nRecs()){
      db.write(record, DB_REC eventlogRecord);
  }else{
    db.append(DB_REC eventlogRecord);
  }
  
  eepromSaveLoggerPointer(pointer);
}

boolean eventTriggeredToday(int evnt){
  
  time_t t = now();
  
  
  Record eventlogRecord;
  
    for (int recno = 1; recno <= 10; recno++)
    {
      db.read(recno, DB_REC eventlogRecord);
      //if(eventlogRecord.eType == evnt && eventlogRecord.dYear == year() & eventlogRecord.dMonth == month() && eventlogRecord.dDay == day()){
      //  return true;
      //}

      if(eventlogRecord.eType == evnt && eventlogRecord.tTime > t - 86400){
          return true;
      }
      
    }
    return false;
}


/*void loggerGetAllEvents(Record* allEventlogRecords)
{
    

     uint8_t pointer = EEPROM.read(eventlogPointerAddr);

     for (int recno = pointer - 98; recno <= 10; recno++)// 1 to 10
     {
       db.read(recno, DB_REC eventlogRecord);
       //sprintf(event, "[%d,%d,%d,\"%02d\",\"%02d\",%d],", eventlogRecord.dYear, eventlogRecord.dMonth, eventlogRecord.dDay, eventlogRecord.tHour, eventlogRecord.tMinute, eventlogRecord.eType);
       //strcat (events, event);
       allEventlogRecords[recno-1] = eventlogRecord;
     }
    
     for (int recno = 1; recno < pointer - 98; recno++)
     {
       db.read(recno, DB_REC eventlogRecord);
       //sprintf(event, "[%d,%d,%d,\"%02d\",\"%02d\",%d],", eventlogRecord.dYear, eventlogRecord.dMonth, eventlogRecord.dDay, eventlogRecord.tHour, eventlogRecord.tMinute, eventlogRecord.eType);
       //strcat (events, event);
       allEventlogRecords[recno-1] = eventlogRecord;
     }
     
}*/




void debugDatabase(){
    Record eventlogRecord;
    uint8_t pointer = eepromLoadLoggerPointer();
    for (int recno = 1; recno <= 10; recno++)// 1-10
    {
      db.read(recno, DB_REC eventlogRecord);


    }
}

