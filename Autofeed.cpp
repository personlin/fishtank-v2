#include <Arduino.h>
#include "Enums.h"
#include "Globals.h"

#include "Functions.h"
#include "Autofeed.h"
#include "Voice.h"
#include "Logger.h"

#line 1 "src/Autofeed.ino"

extern const byte lidMotorAPin;// Drives lid motor
extern const byte lidMotorBPin;// Drives lid motor
extern const byte lidOpenSwitchPin;
extern const byte feederTriggerPin;


uint8_t lidMotorSpeed = 180;// 0 - 255


void initAutofeed(){

  
}


void feederTest(){
  sayIt("Forwards", true);
  digitalWrite(lidMotorAPin, LOW);
  analogWrite(lidMotorBPin, 120);
  delay(5000);
  digitalWrite(lidMotorBPin, LOW);
  delay(100);
  sayIt("Stop", true);
  delay(1000);

  sayIt("Reverse", true);
  digitalWrite(lidMotorBPin, LOW);
  analogWrite(lidMotorAPin, 120);
  delay(5000);
  digitalWrite(lidMotorAPin, LOW);
  delay(100);
  sayIt("Stop", true);
  delay(1000);
  feederTest();
}


void AutoFeedTheFish(){
  feedTheFish(AUTO);
}

void feedTheFish(int mode){


  closeLid();
 
  if(!openLid()){
   sayIt("I cannot open my lid. I could not feed the fish.", true);
   return; 
  }
    
  triggerFeeder();
 
    // Log the feed event
    if(mode == AUTO){
      logEvent(eventAutoFeed, false);
    }else if(mode == MANUAL){
      logEvent(eventManualFeed, false);
    }// Otherwise, debug, dont log
  
  delay(10000);

  //close lid
  closeLid();// FAULT: actually opens again

}

void triggerFeeder(){
  
    // Pulse trigger low for 1/4 second to trigger feeder
    //pinMode(feederTriggerPin , OUTPUT);// High impedance
    digitalWrite(feederTriggerPin, HIGH);
    delay(250);
    digitalWrite(feederTriggerPin, LOW);
  
}



void closeLid(){
  
    digitalWrite(lidMotorBPin, LOW);
  
    // Fast-start motor to get it up to speed
    analogWrite(lidMotorAPin, 220);
    delay(100);
    
    analogWrite(lidMotorAPin, lidMotorSpeed);
    delay(3000);
    
    digitalWrite(lidMotorAPin, LOW);
    digitalWrite(lidMotorBPin, LOW);
}


boolean openLid(){
  
  int i;
  if(lidIsFullyOpen())debug("already open");
  if(lidIsFullyOpen())return true;// Already open
  
  digitalWrite(lidMotorAPin, LOW);
  
  // Fast-start motor to get it up to speed
  analogWrite(lidMotorBPin, 220);
  delay(100);
  
  analogWrite(lidMotorBPin, lidMotorSpeed);
  
  // Keep opening for 5 seconds or until lid is open
  for(i=0; i<5000; i++){
    if(lidIsFullyOpen())break;
    delay(1);
  }
  
  digitalWrite(lidMotorAPin, LOW);
  digitalWrite(lidMotorBPin, LOW);
  
  if(!lidIsFullyOpen()){
      return false;
  }
  return true;
}

boolean lidIsFullyOpen(){
  return digitalRead(lidOpenSwitchPin) == HIGH;
}

