#include <Arduino.h>
#include <CmdMessenger.h>  // CmdMessenger
#include "Serial.h"
#include "Eepromy.h"
#include "Matrix.h"
#include "Timers.h"
#include "Functions.h"
#include "Autofeed.h"
#include "Voice.h"
#include "Lights.h"

#line 1 "src/Serial.ino"
// TODO: Constrain volume to -48 to 18

//#include <CmdMessenger.h>  // CmdMessenger

// ################# External global variables #################




// ################# Global Variables #################
char serialFieldSeparator = ',';
char serialCommandSeparator = ';';


// ################# Objects #################
CmdMessenger cmdMessenger = CmdMessenger(Serial, serialFieldSeparator, serialCommandSeparator);


// This is the list of recognized commands. These can be commands that can either be sent or received. 
// In order to receive, attach a callback function to these events
enum
{
  // Commands
  kShowHelp,
  kSetLightLevel, // Command to 
  kSetScreenLevel, // Command to 
  kSetLightOnTime, // Command to 
  kSetLightOffTime, // Command to 
  kSetServoUp, // Command to 
  kSetServoDown, // Command to 
  kTriggerFeed, // Command to 
  kSetFeedEnable, // Command to 
  kSetFeedDisable, // Command to 
  kMessageMatrix, // Command to 
  kMessageVoice, // Command to 
  kSetVoiceVolume,
  kSetIpAddress,
  kSetMultiple,
  kSetTime,
  kGetData,   // Request all status info
  kGetTime,
  // Send
  kAcknowledge  , // Command to acknowledge that cmd was received
  kData,// All current status data
  kError   , // Command to report errors
  kRequestTime
};


void initSerial()
{
  Serial.begin(9600);

  // Adds newline to every command
  cmdMessenger.printLfCr();   

  // Attach my application's user-defined callback methods
  attachCommandCallbacks();

  // Send the status to the PC that says the Arduino has booted
  //cmdMessenger.sendCmd(kAcknowledge,"Arduino has started!");
}


// Commands we send from the PC and want to receive on the Arduino.
// We must define a callback function in our Arduino program for each entry in the list below.

void attachCommandCallbacks()
{
  // Attach callback methods
  cmdMessenger.attach(OnUnknownCommand);
  cmdMessenger.attach(kShowHelp, serialFunctionShowhelp);
  cmdMessenger.attach(kSetLightLevel, serialFunctionLightlevel);
  cmdMessenger.attach(kSetScreenLevel, serialFunctionScreenlevel);
  cmdMessenger.attach(kSetLightOnTime, serialFunctionLightontime);
  cmdMessenger.attach(kSetLightOffTime, serialFunctionLightofftime);
  cmdMessenger.attach(kSetServoUp, serialFunctionServoup);
  cmdMessenger.attach(kSetServoDown, serialFunctionServodown);
  cmdMessenger.attach(kTriggerFeed, serialFunctionFeed);
  cmdMessenger.attach(kSetFeedEnable, serialFunctionFeedenable);
  cmdMessenger.attach(kSetFeedDisable, serialFunctionFeeddisable);
  cmdMessenger.attach(kMessageMatrix, serialFunctionMessagematrix);
  cmdMessenger.attach(kMessageVoice, serialFunctionMessagevoice);
  cmdMessenger.attach(kSetVoiceVolume, serialFunctionVoicevolume);
  cmdMessenger.attach(kSetIpAddress, serialFunctionSetipaddress);
  cmdMessenger.attach(kSetMultiple, serialFunctionSetmultiple);
  cmdMessenger.attach(kSetTime, serialFunctionSettime);
  cmdMessenger.attach(kGetData, serialFunctionGetdata);
  cmdMessenger.attach(kGetTime, serialFunctionGettime);
}

// ------------------  C A L L B A C K S -----------------------

void serialFunctionShowhelp()
{
/*
0: Show Help
1: Set light level
2: Set screen brightness
3: Set light on time
4: Set lights off time
5: Servo up
6: Servo down
7: Trigger feeder
8: Enable auto-feeder
9: Disable auto-feeder
10: Display matrix message
11: Speak message
12: Set voice volume
13: Set IP address#
14: Set multiple settings at once
15: Set time (timestamp)
16: Send back all data
17: Get current time
*/

  cmdMessenger.sendCmd(kAcknowledge,"\r\n0: Show help\r\n1: Set light level (0-255)\r\n2: Set screen brightness (0-15)\r\n3: Set lights on time (mins)\r\n4: Set lights off time (mins)\r\n5: Servo up\r\n6: Servo down\r\n7: Trigger feeder\r\n8: Enable auto-feeder\r\n9: Disable auto-feeder\r\n10: Display matrix message\r\n11: Speak message\r\n12: Set voice volume (0-66)\r\n13: Set IP address\r\n14: Set light on time, light off time, feed time, autofeed enabled, volume\r\n15: Set time by timestamp\r\n16: Send back all data (temp, waterlevel, lidisopen, lights on hour, min, lights off hour, min)\r\n17: Get current time\r\n\r\nCommands must be in format: [Command No.],[Parameters];\r\nCTRL+A, CTRL+X to exit.");
}
/*
  cmdMessenger.sendCmdArg(readTemperature());
  cmdMessenger.sendCmdArg(getWaterLevel());
  cmdMessenger.sendCmdArg(lidIsFullyOpen());
  cmdMessenger.sendCmdArg(getTimeOnHour());
  cmdMessenger.sendCmdArg(getTimeOnMinute());
  cmdMessenger.sendCmdArg(getTimeOffHour());
  cmdMessenger.sendCmdArg(getTimeOffMinute());
*/

// Called when a received command has no attached function
void OnUnknownCommand()
{
  cmdMessenger.sendCmd(kError,"Command without attached callback");
}

// Callback function that responds that Arduino is ready (has booted up)
void serialArduinoReady()
{
  cmdMessenger.sendCmd(kAcknowledge,"Arduino ready");
}


void serialFunctionLightlevel()
{
  cmdMessenger.sendCmd(kAcknowledge,"Setting light level");
          uint8_t level = cmdMessenger.readIntArg();
          setLightLevel(level);
          eepromSaveLightLevel(level);
          controlLights();

      char buf[2];
      itoa(level, buf, 10);
      char message[24] = "Light level set to: ";
      strcat(message, buf);
      strcat(message, '\0');
      cmdMessenger.sendCmd(kAcknowledge, message);
}

void serialFunctionScreenlevel()
{
  
      uint8_t level = cmdMessenger.readIntArg();
      matrixSetMaxBrightness(level);
      eepromSaveMatrixMaxBrightness(level);

      

      char buf[2];
      itoa(level, buf, 10);
      char message[29] = "Screen brightness set to: ";
      strcat(message, buf);
      strcat(message, '\0');
      cmdMessenger.sendCmd(kAcknowledge, message);
}

void serialFunctionLightontime()
{
  long timeVal = cmdMessenger.readIntArg();
  setLightsOnTime(timeVal);


  char message[29];
  sprintf(message, "Lights on time set to: %d:%d", getTimeOnHour(), getTimeOnMinute());
  strcat(message, '\0');


  cmdMessenger.sendCmd(kAcknowledge, message);
}

void serialFunctionLightofftime()
{
      long timeVal = cmdMessenger.readIntArg();
      setLightsOffTime(timeVal);

  char message[29];
  sprintf(message, "Lights off time set to: %d:%d", getTimeOffHour(), getTimeOffMinute());
  strcat(message, '\0');
}

void serialFunctionServoup()
{
        pumpUp();
}

void serialFunctionServodown()
{
        pumpDown();
}

void serialFunctionFeed()
{
      feedTheFish(MANUAL);
}

void serialFunctionFeedenable()
{
      setAutoFeedEnabled(true);
}

void serialFunctionFeeddisable()
{
      setAutoFeedEnabled(false);
}

void serialFunctionMessagematrix()
{
      char buf[350] = { '\0' }; 
      
      cmdMessenger.copyStringArg (buf, 350);
      cmdMessenger.unescape(buf);

      matrixScrollMessage(buf, 150, 2);
}

void serialFunctionMessagevoice()
{
      char buf[350] = { '\0' };
      cmdMessenger.copyStringArg (buf, 350);
      cmdMessenger.unescape(buf);

    // Say it
    sayIt(buf, false);
}

void serialFunctionVoicevolume()
{
      int8_t vol = cmdMessenger.readIntArg();
      setVolume(vol);
      eepromSaveVolume(vol);
}

void serialFunctionSetipaddress()
{
  char buf[16] = {0};
   strcpy(buf, cmdMessenger.readStringArg());
  setIpAddress(buf);
}

void serialFunctionSetmultiple()
{
 
  int onTime = cmdMessenger.readIntArg();
  setLightsOnTime(onTime);

  int offTime = cmdMessenger.readIntArg();
  setLightsOnTime(offTime);

  int feedTime = cmdMessenger.readIntArg();
  setFeedTime(feedTime);

  setAutoFeedEnabled(cmdMessenger.readIntArg());

}

void serialFunctionSettime()
{
  unsigned long curTime = cmdMessenger.readFloatArg();

  setCachedTime(curTime);


  // Return message
  char buf[22];
  char message[36] = "Time set to: ";
  getDateTimeString(1, buf);
  strcat(message, buf);
  strcat(message, '\0');
  cmdMessenger.sendCmd(kAcknowledge,message);
}

void serialFunctionGetdata()
{
  
  //loggerGetAllEvents(allEventlogRecords);
  
  // temp, 
  cmdMessenger.sendCmdStart(kData);
  cmdMessenger.sendCmdArg(readTemperature());
  cmdMessenger.sendCmdArg(getWaterLevel());
  cmdMessenger.sendCmdArg(lidIsFullyOpen());
  cmdMessenger.sendCmdArg(getTimeOnHour());
  cmdMessenger.sendCmdArg(getTimeOnMinute());
  cmdMessenger.sendCmdArg(getTimeOffHour());
  cmdMessenger.sendCmdArg(getTimeOffMinute());
  cmdMessenger.sendCmdEnd();
  
}

void serialFunctionGettime()
{
  char message[37];

  if(getHaveTime()){
    char buf[22];
    strcpy(message, "Current time: ");
    getDateTimeString(1, buf);
    strcat(message, buf);

  }else{
    strcpy(message, "Time is not set");
  }
  strcat(message, '\0');
  cmdMessenger.sendCmd(kAcknowledge,message);
}

void serialHandleData()
{
  cmdMessenger.feedinSerialData();
}



void requestTimeFromSerial()
{
  cmdMessenger.sendCmd(kRequestTime, "Time please");

}