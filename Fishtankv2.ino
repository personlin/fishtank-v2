

#include <SPI.h>
#include <serLCD.h>// LCD
#include <Time.h>  
#include <TimeAlarms.h> 
#include <Timezone.h> 
#include <CmdMessenger.h>

//#include <MemoryFree.h>

#include "Enums.h"
#include "Eepromy.h"
#include "Functions.h"
#include "Globals.h"
#include "Lcd.h"
#include "Lights.h"
#include "Logger.h"
#include "Matrix.h"
#include "Serial.h"
#include "Voice.h"
#include "Timers.h"
#include "Autofeed.h"




// http://arduino.cc/en/Tutorial/UdpNtpClient

/* ################  Notes ################
Use of Servo library disables PWM on pins 9 and 10.

*/

/* ################  Todo ################
// Fix 'recno out of range'.
// Sent time/date via serial
// sayDemonstration() needs doing
// LCD needs to use new lcdDisplay function


*/



// ################# External global variables #################
extern const byte buttonPin;


// ################# Global Variables #################
boolean debugMode = false; 


/* ####################################################################################
   #                                                                                  #
   #                               Instantiate objects                                #
   #                                                                                  #
   #################################################################################### */

  // Objects are now instansiated within each relevant cpp file







/* ####################################################################################
   #                                                                                  #
   #                                  Setup Procedure                                 #
   #                                                                                  #
   #################################################################################### */

void setup() {

  initLcd();
  lcdDisplay("Starting", 1);
  lcdDisplay("> Hardware...", 2);
  
  // Inputs
  initPins();

  // Load configuration settings from EEPROM
  initEeprom();

  // Serial for debugging:
  initSerial();
  
  
  initLights();

  //eepromSaveVolume(60);

  // Voice module serial
  initVoice();
 
  //feederTest();

  debug("Setting up Matrix...");

  // Matrix display
  initMatrix();
  
  flashLed(3);
  delay(3000);
  // Enter debug mode if button is held down
  if(digitalRead(buttonPin) == LOW || debugMode == true)debugLoop();

 
//lcdFreeMemory();
 
  debug("Loading configuration from memory...");



  initDb();
  

  
  lcdDisplay("> Time/Date...  ", 2);
  debug("Getting time..." );

  // Get the time and schedule daily updates
  initTime();

  debug("Setting alarms" );
   
  initTimers();



  delay(10);


  
  logEvent(eventPowerOn, true);
  
  
  debugDatabase();
  
  lcdDisplay("> Complete!     ", 2);
  
  debug("Loaded.");
  
  delay(500);

  controlLights();
  delay(10);
   
   // Speak a startup message
  sayStartupMessage();
   
  matrixSplashScreen();// < ------------------------------ Put back in at go-live!
  
}


/* ####################################################################################
   #                                                                                  #
   #                                     Main Loop                                    #
   #                                                                                  #
   #################################################################################### */

void loop() {

  //controlLights();
  
  checkLcdButton();// Switch between info pages
  
  fadeLed();// LED fades in and out

  serialHandleData();

  Alarm.delay(40);  
   
}



void debugLoop(){
  sayIt("Starting up", true);
  delay(3000);

  //debug("-= Debug mode =-");
  serialArduinoReady();


  while(1)// digitalRead(buttonPin) == HIGH
  {
    serialHandleData();
    checkLcdButton();// Switch between info pages
    fadeLed();// LED fades in and out
    Alarm.delay(40);
  }
/*
  debug("Flashing LED");
  delay(1000);
  flashLed(5);
  delay(3000);

  debug("LCD Message");
  delay(1000);
  lcdDisplay("Line 1 test", 1);
  lcdDisplay("Line 2 test", 2);
  delay(3000);

  debug("LED Matrix");
  delay(1000);
  matrixDisplayMessage("MATRIX", 2, 0, 0);
  delay(3000);

  debug("Lights");
  delay(1000);
  analogWrite(lightsPin, 254);
  delay(5000);
  analogWrite(lightsPin, 0);
  delay(3000);

  debug("Temp");
  delay(1000);
  int tmp = readTemperature();
  char buf[17];
  sprintf(buf, "Temperature: %dC", tmp);
  debug(buf);
  delay(3000);

  debug("Water level");
  delay(1000);
  int lev;
  for(int i = 0; i < 5; i++){
    lev = getWaterLevel();
    char buf[15];
    sprintf(buf, "Level: %d", lev);
    debug(buf);
    delay(100);
  }
  delay(3000);


  debug("Servo");
  delay(1000);
  pumpUp();
  delay(200);
  pumpUp();
  delay(1000);
  pumpDown();
  delay(200);
  pumpDown();
  delay(3000);
  

  debug("Voice");
  delay(1000);
  sayIt("Voice test. This is an example message to test volume, rate and tone", true);
  delay(3000);


  debug("Lid open, trigger feeder, lid close");
  delay(1000);
  feedTheFish(DEBUG);
  delay(5000);


  debugLoop();
*/
  
}









