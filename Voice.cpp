#include <Arduino.h>
#include <SoftwareSerial.h>// Note: SoftwareSerial is now a clone of NewSoftSerial, so no need to use NewSoftSerial.
#include <Time.h>  
#include <TimeAlarms.h> 
#include <Timezone.h> 
#include "Eepromy.h"
#include "Voice.h"
#include "Globals.h"



extern const byte talkTxPin;
extern const byte talkRxPin;

// ################# Global Variables #################
boolean talkModuleAvailable = false;

// ################# Objects #################
SoftwareSerial emicSerial =  SoftwareSerial(talkRxPin, talkTxPin);// Voice module serial comms




void initVoice()
{
  emicSerial.begin(9600);
  delay(10);
  emicSerial.flush();// Flush the receive buffer
  delay(10);
  talkModuleAvailable = true;

  int vol = eepromLoadVolume();
  setVolume(vol);
}

void setVolume(int8_t vol)// 0 - 66
{

  vol = constrain(vol, 0, 66) - 48;// -48 to 18
   
  waitForTalkModuleAvailable();

  
  // Set volume
  // -48 to 18. Default is 0
  emicSerial.print('V');
  emicSerial.println(vol, DEC);
  //emicSerial.print('\n');
  
  while (emicSerial.read() != ':');
  
}


void sayStartupMessage()
{
  Serial.println("Saying startup msg " );
  time_t t = now();
 
  if(timeStatus() == timeNotSet){
    sayIt("hello", false);
  }
  else if(hour(t) < 12){
    sayIt("good morning", false);
  }else if(hour(t) < 17)
    sayIt("good afternoon", false);
  else{
    sayIt("good evening", false);
  }
  
  
}


void sayIt(char message[], boolean waitForCompletion)
{
  Serial.println("Waiting" );
  waitForTalkModuleAvailable();
  Serial.print("Saying: " );
  Serial.println(message );
  emicSerial.print('S');
  emicSerial.print(message);  // Send the desired string to convert to speech
  emicSerial.print('\n');
  
  talkModuleAvailable = false;
  
  if(waitForCompletion){
    waitForTalkModuleAvailable();
  }
}

void waitForTalkModuleAvailable()
{
  if(talkModuleAvailable)return;
  
  while (emicSerial.read() != ':');
  
  talkModuleAvailable = true;
}



