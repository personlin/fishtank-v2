#ifndef _Matrix_h
#define _Matrix_h

#include "Enums.h"

void initMatrix();
void matrixSetMaxBrightness(uint8_t level);
void matrixFadeIn();
void matrixFadeOut();
void matrixDisplayMessage(char message[], byte fontSize, byte row, byte col);
void matrixSplashScreen();
void matrixClockDisplay();
void matrixScrollMessage(char message[], int8_t speed, int8_t fontSize);
void matrixClearScreen();
void matrixWriteScreen();
void matrixSetBrightness();
matrixMode getMatrixCurrentlyShowing();
void setMatrixCurrentlyShowing(matrixMode mode);

#endif