#ifndef _Logger_h
#define _Logger_h

void initDb();
void logEvent(int evnt, boolean force);
boolean eventTriggeredToday(int evnt);
void debugDatabase();

#endif