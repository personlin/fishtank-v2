#ifndef _Voice_h
#define _Voice_h

void initVoice();
void setVolume(int8_t vol);
void sayStartupMessage();
void sayIt(char message[], boolean waitForCompletion);
void waitForTalkModuleAvailable();

#endif