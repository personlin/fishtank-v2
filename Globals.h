

/* ####################################################################################
   #                                                                                  #
   #                                  Pin Assignments                                 #
   #                                                                                  #
   #################################################################################### */

const byte lightsPin           = 12; // Analog output pin that the LED is attached to
const byte servoPin            = 30; // Analog output pin that the pump tap servo is attached to
const byte tempPin             = 47; // pin that the temperature sensor is attached to
const byte buttonPin           = 35;
const byte matrixCsPin         = 6;// Chip select 0
const byte matrixCs2Pin        = 28;// Chip select 1
const byte matrixDataPin       = 8;
const byte matrixWrPin         = 9;// Write
const byte ledPin              = 13;// Also PCB LED
const byte moistureEnablePin   = A3;// Bring high to read I think// TODO: remove completely
const byte moisturePin         = A4;// TODO: remove completely
const byte lightSensorPin      = A9;  // LDR sensor, detects lid removed// TODO: connect up or remove completely
const byte sonarTriggerPin     = 45;// Sonar
const byte sonarEchoPin        = 44;// Sonar
const byte lcdPin              = 5;  // LCD Data
const byte talkTxPin           = 48;// Connected to Emic2 SIN
const byte talkRxPin           = 53;// Connected to Emic2 SOUT
const byte lidMotorAPin        = 2;// Drives lid motor
const byte lidMotorBPin        = 3;// Drives lid motor
const byte lidOpenSwitchPin    = 31;// Microswitch to disable motor once lid is closed <=== Unsure if correct pin, or pin description
const byte feederTriggerPin    = 7;// bring low to trigger a feed
