#ifndef _Enums_h
#define _Enums_h

#include <Time.h>

// Type of event to be logged in the database
enum eventType {
  eventLowTemp,
  eventHighTemp,
  eventLowWater,
  eventLeak,
  eventPowerOn,
  eventManualFeed,
  eventAutoFeed,
  eventFeedError
};

enum feedMode{
  AUTO,
  MANUAL,
  DEBUG
};

// An entry in the event log database
struct Record {
  uint16_t dYear;
  int8_t dMonth;
  int8_t dDay;
  int8_t tHour;
  int8_t tMinute;
  int eType;
  time_t tTime;
};

enum matrixMode {
  matrixOff,
  matrixBlank,
  matrixSplash,
  matrixTimeDate,
  matrixScrollingMessage,
  matrixLeakAlarm,
  feeding
};

#endif
