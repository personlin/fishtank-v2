#include <Arduino.h>
#include <Time.h>  
#include <TimeAlarms.h> 
#include <Timezone.h> 
#include "Eepromy.h"
#include "Globals.h"
#include "Timers.h"
#include "Lights.h"
#include "Autofeed.h"
#include "Matrix.h"
#include "Lcd.h"

boolean haveTime = false;// True if we succesfully loaded time from NTP
int8_t onTimerId;
int8_t offTimerId;
int8_t feedTimerId;
int8_t clockTimerId;

uint8_t timeOnHour = 0;
uint8_t timeOnMinute = 0;
uint8_t timeOffHour = 0;
uint8_t timeOffMinute = 0;

boolean autoFeedEnabled = false;
uint8_t autoFeedHour = 0;
uint8_t autoFeedMinute = 0;

void initTimers()
{

  eepromLoadLightsOnTime(&timeOnHour, &timeOnMinute);
  eepromLoadLightsOffTime(&timeOffHour, &timeOffMinute);

  autoFeedEnabled = eepromLoadAutofeedEnabled();
  eepromLoadAutofeedTime(&autoFeedHour, &autoFeedMinute);

  onTimerId = Alarm.alarmRepeat(getTimeOnHour(),getTimeOnMinute(),0, lightsOn);  // Lights on in the morning
  offTimerId = Alarm.alarmRepeat(getTimeOffHour(),getTimeOffMinute(),0,lightsOff);  // Lights off in the evening
  feedTimerId = Alarm.alarmRepeat(getAutoFeedHour(),getAutoFeedMinute(),0,AutoFeedTheFish);  // Feed the fish every day
    Serial.print("On id: ");
    Serial.println(onTimerId);

  clockTimerId = Alarm.timerRepeat(5, matrixClockDisplay);// Display time every minute

  // Lights on/off time
  /* <== Allow timers to run anyway, so at least fish will get fed!
  if(!getHaveTime()){
    Alarm.disable(feedTimerId);
    Alarm.disable(clockTimerId);
  }
  */

  Alarm.timerRepeat(10, lcd_show_info);// Display time every 10 secs
}


// void setHaveTime(boolean val){
// 	haveTime = val;
// }

boolean getHaveTime(){
	return timeStatus() != timeNotSet;
}


void setLightsOnTime(int totalMins){

    
  timeOnHour = totalMins/60;// Rounded down when cast to int
  timeOnMinute = totalMins % 60;
  
  Alarm.write(onTimerId, AlarmHMS(timeOnHour,timeOnMinute,0));
  
  Serial.print(timeOnHour);
  Serial.print(":");
  Serial.println(timeOnMinute);

  //eepromWrite(timeOnHourAddr, timeOnHour);
  //eepromWrite(timeOnMinuteAddr, timeOnMinute);
  eepromSaveLightsOnTime(timeOnHour, timeOnMinute);
  
  controlLights();
}
uint8_t getTimeOnHour(){
  return timeOnHour;
}
uint8_t getTimeOnMinute(){
  return timeOnMinute;
}

uint8_t getLightsOnTime(){
  return timeOnHour * 60 + timeOnMinute;
}


void setLightsOffTime(int totalMins){

  
  timeOffHour = totalMins/60;// Rounded down when cast to int
  timeOffMinute = totalMins % 60;

  Alarm.write(offTimerId, AlarmHMS(timeOffHour,timeOffMinute,0));
  Serial.print(timeOffHour);
  Serial.print(":");
  Serial.println(timeOffMinute);
  eepromSaveLightsOffTime(timeOffHour, timeOffMinute);
  
  controlLights();
}
uint8_t getTimeOffHour(){
  return timeOffHour;
}
uint8_t getTimeOffMinute(){
  return timeOffMinute;
}

uint8_t getLightsOffTime(){
  return timeOffHour * 60 + timeOffMinute;
}

boolean getAutoFeedEnabled(){
  return autoFeedEnabled;
}
void setAutoFeedEnabled(boolean val){
  if(val){
    Alarm.enable(feedTimerId);
    eepromSaveAutofeedEnabled(true);
  }else{
    Alarm.disable(feedTimerId);
    eepromSaveAutofeedEnabled(false);
  }
  autoFeedEnabled = val;
}


void setFeedTime(uint8_t totalMins){
  
  autoFeedHour = totalMins/60;// Rounded down when cast to int
  autoFeedMinute = totalMins % 60;

  Alarm.write(feedTimerId, AlarmHMS(autoFeedHour,autoFeedMinute,0));

  eepromSaveAutofeedTime(autoFeedHour, autoFeedMinute);
}
uint8_t getAutoFeedHour(){
  return autoFeedHour;
}
uint8_t getAutoFeedMinute(){
  return autoFeedMinute;
}