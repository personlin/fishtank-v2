#ifndef _Autofeed_h
#define _Autofeed_h

void feederTest();
void AutoFeedTheFish();
void feedTheFish(int mode);
void triggerFeeder();
void closeLid();
boolean openLid();
boolean lidIsFullyOpen();

#endif