#include <Arduino.h>
#include <TinyMatrix.h>// Matrix
#include "Enums.h"
#include "Globals.h"
#include "Functions.h"

#line 1 "src/Matrix.ino"
// TODO: restore HT1632





extern const byte matrixCsPin;// Chip select 0
extern const byte matrixCs2Pin;// Chip select 1
extern const byte matrixDataPin;
extern const byte matrixWrPin;// Write



// ################# Global Variables #################
int8_t matrixFullBrightness = 16;
int8_t matrixCurrentBrightness = matrixFullBrightness;
char* matrixMessageQueue[6];
byte lastDisplayedMinute = 0;// For matrix



matrixMode matrixCurrentlyShowing = matrixOff;


// ################# Objects #################
TinyMatrix matrix = TinyMatrix(matrixDataPin, matrixWrPin, matrixCsPin, matrixCs2Pin);// LED Matrix


void initMatrix()
{
  matrix.begin(HT1632_COMMON_16NMOS); 
  matrix.clearScreen();

  matrixCurrentBrightness = matrixFullBrightness;
  matrix.setBrightness(matrixCurrentBrightness);
  matrix.writeScreen();
  delay(10);
}


void matrixSetMaxBrightness(uint8_t level){
  matrixFullBrightness = level;
  matrixCurrentBrightness = matrixFullBrightness;

  matrix.setBrightness(level);
}


void matrixFadeIn(){
  if(matrixCurrentlyShowing == matrixOff)return;
  
  for (int8_t i=0; i<=matrixCurrentBrightness; i++) {
   matrix.setBrightness(i);
   delay(100);
  }
}
void matrixFadeOut(){
  // Adjust the brightness down 
  for (int8_t i=matrixCurrentBrightness; i>=0; i--) {
   matrix.setBrightness(i);
   delay(100);
  }
}

void matrixDisplayMessage(char message[], byte fontSize, byte row, byte col){
  
  if(matrixCurrentlyShowing == matrixOff)return; 
  
  int8_t charWidth = 5;
  int8_t charHeight = 8;
  
  matrix.setTextSize(fontSize);
  matrix.setTextColor(1);   // 'lit' LEDs
  matrix.setBrightness(matrixCurrentBrightness);
  matrix.clearScreen();
  

  
  int8_t x = (col * (charWidth*fontSize)) + 1;
  int8_t y = (row * (charHeight*fontSize)) + 1;
  matrix.setCursor(x, y);   // start at top left, with one pixel of spacing
  matrix.print(message);

  matrix.writeScreen();

}



void matrixSplashScreen(){
  if(matrixCurrentlyShowing == matrixOff)return;
  
  matrixCurrentlyShowing = matrixSplash;
  
  matrix.setTextSize(1);
  matrix.setTextColor(1);   // 'lit' LEDs
  matrix.setBrightness(matrixCurrentBrightness);
  matrix.clearScreen();
 
   char firstWord[9] = "Bluereef";//{'B','l','u','e','l','e','a','f'};
   char secondWord[9] = "Aquarium";//{'F','i','s','h','t','a','n', 'k'};
 
   int8_t cursorPos = 0;
 
 
   matrix.fillRect(0, 0, 5, 8, 1);
   matrix.writeScreen();
   delay(400);
 
   // Print first Word
   for(int8_t i = 0; i < 8; i++){
     
     matrix.fillRect(cursorPos, 0, 5, 8, 0);// Remove the previous block where the letter will go
     
     matrix.setCursor(cursorPos, 0);
     matrix.print(firstWord[i]);
     //matrix.writeScreen();

     cursorPos += 6;
     matrix.fillRect(cursorPos, 0, 5, 8, 1);
     matrix.writeScreen();
     delay(200);
   }
 
 //delay(300);
 
 // Flash the cursor a few times
 for(int8_t i = 0; i < 2; i++){
   matrix.fillRect(cursorPos, 0, 5, 8, 0);
   matrix.writeScreen();
   delay(400);
   matrix.fillRect(cursorPos, 0, 5, 8, 1);
   matrix.writeScreen();
   delay(400);
 }
 
 matrix.fillRect(cursorPos, 0, 5, 8, 0);// Remove cursor
 cursorPos = 0;// Move cursor to start
 
  // Flash the cursor a few times on second row
 for(int8_t i = 0; i < 2; i++){
   matrix.fillRect(cursorPos, 8, 5, 8, 0);
   matrix.writeScreen();
   delay(400);
   matrix.fillRect(cursorPos, 8, 5, 8, 1);
   matrix.writeScreen();
   delay(400);
 }
 
   // Print second Word
   for(int8_t i = 0; i < 8; i++){
     
     matrix.fillRect(cursorPos, 8, 5, 8, 0);// Remove the previous block where the letter will go
     
     matrix.setCursor(cursorPos, 8);
     matrix.print(secondWord[i]);
     //matrix.writeScreen();

     cursorPos += 6;
     matrix.fillRect(cursorPos, 8, 5, 8, 1);
     matrix.writeScreen();
     delay(200);
   }
 
 //delay(300);
 
 // Flash the cursor a few times
 for(int8_t i = 0; i < 8; i++){
   matrix.fillRect(cursorPos, 8, 5, 8, 0);
   matrix.writeScreen();
   delay(400);
   matrix.fillRect(cursorPos, 8, 5, 8, 1);
   matrix.writeScreen();
   delay(400);
 }
 
 
 
 
  matrixFadeOut();
  
  matrix.clearScreen();
  matrix.writeScreen();
  
  matrixCurrentlyShowing = matrixBlank;
}


void matrixClockDisplay(){

  if(matrixCurrentlyShowing != matrixBlank && matrixCurrentlyShowing != matrixTimeDate)return;// Showing something else
  if(matrixCurrentlyShowing == matrixTimeDate && lastDisplayedMinute == minute())return;
  
  boolean wasBlank = (matrixCurrentlyShowing == matrixBlank)?true:false;
//if(wasBlank)Serial.println("Was Blank!");
  matrixCurrentlyShowing = matrixTimeDate;
  
  char curtime[6];
  char curdate[9];

  getDateTimeString(2, curtime);
  getDateTimeString(4, curdate);
  
  matrix.setTextSize(1);
  matrix.setTextColor(1);   // 'lit' LEDs
  
  matrix.clearScreen();

  matrix.setCursor(0, 0);   // start at top left, with one pixel of spacing
  matrix.print(curtime);
  
  matrix.setCursor(0, 8);   // start at top left, with one pixel of spacing
  matrix.print(curdate);

  if(wasBlank)matrix.setBrightness(0);

  matrix.writeScreen();
  
  if(wasBlank)matrixFadeIn();
  delay(50);
  
    
  lastDisplayedMinute = minute();
  
}




// Scroll a message from right to left
void matrixScrollMessage(char message[], int8_t speed, int8_t fontSize){
  if(matrixCurrentlyShowing == matrixOff)return;
  boolean wasBlank = (matrixCurrentlyShowing == matrixBlank)?true:false;
  
  matrixCurrentlyShowing = matrixScrollingMessage;// Tracks what's on the matrix
  
  if(!wasBlank)matrixFadeOut();
  
  
  
  int8_t screenPixelLength = matrix.width();// 48
  int8_t charWidth = (fontSize == 1?6:12);
  int16_t messagePixelLength = strlen(message) * charWidth;
  
  int8_t yPos = (fontSize == 1?9:1);
  
  int8_t delayms = 255 - speed;
  
  
  
  
  matrix.setTextSize(fontSize);
  matrix.setTextColor(1);   // 'lit' LEDs
  matrix.clearScreen();
  matrix.setBrightness(matrixCurrentBrightness);
  delay(1000);
  
  //for (int16_t i = screenPixelLength - 1; i >= (0 - (messagePixelLength * 1.2 + 4)); i=i-4) {
  for (int16_t i = screenPixelLength - 1; i >= (0 - (messagePixelLength + 4)); i=i-4) {
    

    matrix.clearScreen();
    matrix.setCursor(i, yPos);
    matrix.setTextColor(1);
    
    matrix.print(message);
    matrix.writeScreen();
    delay(delayms);

  }
  debug("Scroll end");
  delay(1000);
  
  matrixCurrentlyShowing = matrixBlank;
  //matrixProcessMessageQueue();
}


void matrixClearScreen()
{
  matrix.clearScreen();
};
void matrixWriteScreen()
{
  matrix.writeScreen();
};

void matrixSetBrightness()
{
  matrix.setBrightness(matrixCurrentBrightness);
}

matrixMode getMatrixCurrentlyShowing(){
  return matrixCurrentlyShowing;
}
void setMatrixCurrentlyShowing(matrixMode mode){
  matrixCurrentlyShowing = mode;
}

/*

void matrixAddMessageToQueue(char message[]){
  for (int8_t i = 0; i < 5; i++) {
    if(matrixMessageQueue[i] == '\0'){// We found an empty element
      matrixMessageQueue[i] = message;
      //strcpy(matrixMessageQueue[i], message);
      break;
    }
  }
  matrixProcessMessageQueue();
}



void matrixProcessMessageQueue(){

  if(matrixCurrentlyShowing != blank && matrixCurrentlyShowing != matrixTimeDate)return;// We're already displaying something
  
  //if(matrixMessageQueue[0] != '\0'){// The queue contains at least 1 message

  while(matrixMessageQueue[0] != '\0'){
    // Grab the first message and shift the others down one (if any)
    char* message = matrixMessageQueue[0];
    
    for (int8_t j = 0; j < 4; j++) {
      //matrixMessageQueue[j] = matrixMessageQueue[j+1];
      Serial.println(matrixMessageQueue[j]);
      strcpy(matrixMessageQueue[j], matrixMessageQueue[j+1]);
    }

    matrixMessageQueue[4] = '\0';
    matrixScrollMessage(message, 150, 2);

    
  }

  if(haveTime==true)matrixClockDisplay(); 

}
*/

