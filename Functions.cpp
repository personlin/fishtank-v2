#include <Arduino.h>
#include <CmdMessenger.h>
#include <OneWire.h>
#include <Servo.h>
#include <Time.h>  
#include <TimeAlarms.h> 
#include <Timezone.h> 
#include "Enums.h"
#include "Globals.h"

#include "Logger.h"
#include "Eepromy.h"
#include "Lights.h"
#include "Serial.h"
#include "Functions.h"



// ################# External global variables #################
extern boolean debugMode;
extern const byte buttonPin;
extern const byte lightSensorPin;
extern const byte moisturePin;
extern const byte lidOpenSwitchPin;
extern const byte talkTxPin;
extern const byte talkRxPin;
extern const byte sonarEchoPin;
extern const byte lightsPin;
extern const byte ledPin;
extern const byte moistureEnablePin;
extern const byte feederTriggerPin;
extern const byte lidMotorAPin;
extern const byte lidMotorBPin;
extern const byte feederTriggerPin;

extern const byte sonarTriggerPin;
extern const byte tempPin;
extern const byte servoPin;

// ################# Global Variables #################
uint8_t lowWaterTrigger = 10;
uint8_t lowTempTrigger = 8;
uint8_t highTempTrigger = 30;

byte ledBrightness = 2;
boolean ledBrighter = true;// getting brighter
byte servoMiddle = 94;


time_t t, utc, local, cachedTime = 0;

char ipAddress[13] = {"0.0.0.0\0"};



// ################# Objects #################
OneWire  ds(tempPin); // Temperature sensor
Servo servo;// Pump for filter/air




// unsigned long readTime()
// {
//   return 1379620057;
// }




// Set up input and output pins
void initPins()
{
  pinMode(buttonPin, INPUT);
  pinMode(lightSensorPin, INPUT);
  pinMode(moisturePin, INPUT);
  pinMode(lidOpenSwitchPin, INPUT);
  pinMode(talkRxPin, INPUT);
  pinMode(sonarEchoPin, INPUT);
  
  // ##### Outputs #####
  pinMode(lightsPin, OUTPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(moistureEnablePin, OUTPUT);
  pinMode(feederTriggerPin, OUTPUT);
  pinMode(lidMotorAPin, OUTPUT);
  pinMode(lidMotorBPin, OUTPUT);
  pinMode(feederTriggerPin , OUTPUT);
  pinMode(talkTxPin, OUTPUT);
  pinMode(sonarTriggerPin, OUTPUT);
}

void initTime()
{
  //if(!debugMode)delay(30000);// Wait for the Pi to boot
  //requestTimeFfromSerial();
  setSyncInterval(10);
  setSyncProvider(getTime);

  uint8_t giveUp = 0;
  while(timeStatus() == timeNotSet)
  {
    
    for(uint8_t i = 0; i < 50; i++){
      serialHandleData();
      delay(100);
    }
    now();
    if(giveUp > 12)break;// 5 x 12 = 60secs
    giveUp++;
  }

  //Moved to setCachedTime. if(timeStatus() != timeNotSet)setSyncInterval(43200);// Have time, so sync daily
}


time_t getTime()
{
  requestTimeFromSerial();

  return getCachedTime();
}

time_t getCachedTime(){
  return cachedTime;
}
void setCachedTime(time_t timestamp){
 cachedTime = timestamp;

 // Just got time, enable timers
 /*
 if(!getHaveTime()){
  Alarm.enable(feedTimerId);
  Alarm.enable(clockTimerId);
 }
 */

 setTime(timestamp);

 setSyncInterval(43200);
}

char* zeroPad(int val, char* buffer){
  
  char buf[18];
  
  if (val < 10) {
    strcpy(buffer,"0");
    itoa(val,buf,10);
    strcat(buffer,buf);
  }else{
    itoa(val,buf,10);
    strcpy(buffer,buf);
  }

  return buffer;
}





// TODO: Use microsecondsToCentimeters
int getWaterLevel(){
// Function faulty.



  int ultrasoundValue = 0;
  unsigned int maxValue = 0;
  //byte numValues = 1;
  //byte values[9];
  
  //for(byte i=0; i < numValues; i++){
    
    digitalWrite(sonarTriggerPin, LOW);
    delayMicroseconds(2); // Wait for 2 microseconds
    digitalWrite(sonarTriggerPin, HIGH); // Send high pulse
    delayMicroseconds(10); // Wait for 5 microseconds
    digitalWrite(sonarTriggerPin, LOW); // Holdoff
    

    ultrasoundValue = pulseIn(sonarEchoPin, HIGH);
    

    ultrasoundValue = constrain(ultrasoundValue, 350, 1950) - 350;// 1600 - 0
    
    ultrasoundValue = 1600 - ultrasoundValue;// Invert 0 to 1600
    

    ultrasoundValue = map(ultrasoundValue, 0, 1600, 0, 100);

    return ultrasoundValue;
    //maxValue += ultrasoundValue;

    //numValues++;
  //}
  
  //if(maxValue/numValues < lowWaterTrigger)logEvent(eventLowWater);// TODO: put back in
  
  //return (maxValue /numValues);

}









int readTemperature(){
  
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];
  int Temp;
  
  ds.reset_search();
  
  if ( !ds.search(addr)) {
	ds.reset_search();
	return 1;
  }

  if ( OneWire::crc8( addr, 7) != addr[7]) {
	//CRC is not valid
	return 2;
  }

  if ( addr[0] != 0x28) {
	//Device is not a DS18S20 family device
	return 3;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44,1);	   // start conversion, with parasite power on at the end

  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.

  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);	   // Read Scratchpad

  for ( i = 0; i < 9; i++) {	     // we need 9 bytes
    data[i] = ds.read();
  }

  Temp=(data[1]<<8)+data[0];//take the two bytes from the response relating to temperature

  Temp=Temp>>4;//divide by 16 to get pure celcius readout

  //next line is Fahrenheit conversion
  //Temp=Temp*1.8+32; // comment this line out to get celcius

  if(Temp < lowTempTrigger)
    logEvent(eventLowTemp, false);
  else if(Temp > highTempTrigger)
    logEvent(eventHighTemp, false);
    
    
  return Temp;
}





void pumpUp(){
  
  servo.attach(servoPin);
  delay(5);
  servo.write(servoMiddle + 75);
  delay(200);
  servo.write(servoMiddle);
  delay(100);
  servo.detach();
}

void pumpDown(){
  servo.attach(servoPin);
  delay(5);
  servo.write(servoMiddle - 75);
  delay(200);
  servo.write(servoMiddle);
  delay(100);
  servo.detach();
}



void debug(char * message){
  //Serial.print(String(freeMemory(),DEC) + "B : ");
  //cmdMessenger.sendCmd(kAcknowledge, message);
}





char* getDateTimeString(int format, char* buffer){
  
  time_t t = now();

  
  char dayBuf[5];
  char monthBuf[5];
  char yearBuf[7];
  char hourBuf[5];
  char minuteBuf[5];
  
  char dayStrBuf[6];
  
  switch(format){
   case 0:// 01/11/2012 16:30
    //buffer = zeroPad(day(t), dayBuf) + "/" + zeroPad(month(t), dayBuf);
    //return zeroPad(day(t)) + '/' + itoa(month(t),monthBuf,10) + '/' + itoa(year(t),yearBuf,10) + ' ' + zeroPad(hour(t)) + ':' + zeroPad(minute(t));
    
    zeroPad(day(t), dayBuf);
    zeroPad(month(t), monthBuf);
    itoa(year(t),yearBuf,10);
    zeroPad(hour(t), hourBuf);
    zeroPad(minute(t), minuteBuf);
    
    
    strcpy (buffer, dayBuf);
    strcat (buffer, "/");
    strcat (buffer, monthBuf);
    strcat (buffer, "//");
    strcat (buffer, yearBuf);
    strcat (buffer, " ");
    strcat (buffer, hourBuf);
    strcat (buffer, ":");
    strcat (buffer, minuteBuf);
    
    break;
    
   case 1:// 17:12 Mon 07 Mar 2012
   
    zeroPad(hour(t), hourBuf);
    zeroPad(minute(t), minuteBuf);
    zeroPad(day(t), dayBuf);
    zeroPad(month(t), monthBuf);
    itoa(year(t),yearBuf,10);
    

    strcpy (buffer, hourBuf);
    strcat (buffer, ":");
    strcat (buffer, minuteBuf);
    strcat (buffer, " ");
    strcat (buffer, dayShortStr(weekday(t)));
    strcat (buffer, " ");
    strcat (buffer, dayBuf);
    strcat (buffer, " ");
    strcat (buffer, monthShortStr(month(t)));
    strcat (buffer, " ");
    strcat (buffer, yearBuf);

     
   
    //return zeroPad(hour(t)) + ':' + zeroPad(minute(t)) + ' ' + dayShortStr(weekday(t)) + ' ' + zeroPad(day(t)) + ' ' + monthShortStr(month(t)) + ' ' + itoa(year(t),yearBuf,10);
    break;
    
   case 2:// 14:25
   
     zeroPad(hour(t), hourBuf);
     zeroPad(minute(t), minuteBuf);
     
     strcpy (buffer, hourBuf);
     strcat (buffer, ":");
     strcat (buffer, minuteBuf);

    break;
    
   case 3:// 01/11/2012
   
    zeroPad(day(t), dayBuf);
    zeroPad(month(t), monthBuf);
    itoa(year(t),yearBuf,10);
   
    strcpy (buffer, dayBuf);
    strcat (buffer, "/");
    strcat (buffer, monthBuf);
    strcat (buffer, "/");
    strcat (buffer, yearBuf);
   

    break;

  case 4:// 01/11/12
    
    zeroPad(day(t), dayBuf);
    zeroPad(month(t), monthBuf);
    //itoa(year(t),yearBuf,10);
   
    //char subYearBuf[3];
   /*
    int8_t yearBuf;

    
    yearBuf = year(t) - 2000;

    char subYearBuf[3];
   
    subYearBuf[0] = yearBuf & 0xff;
    subYearBuf[1] = (yearBuf>>8)  & 0xff;
   */
   int8_t subYearBuf;
   subYearBuf = year(t) - 2000;
   
    itoa(subYearBuf,yearBuf,10);
   
    strcpy (buffer, dayBuf);
    strcat (buffer, "/");
    strcat (buffer, monthBuf);
    strcat (buffer, "/");
    strcat (buffer, yearBuf);
    
  break;

   case 5:// Mon 07 Mar 2012 GMT
   //char buf[5];
    //return dayShortStr(weekday(t)) + ' ' + zeroPad(day(t)) + ' ' + monthShortStr(month(t)) + ' ' + itoa(year(t),buf,10);
    
    zeroPad(day(t), dayBuf);
    itoa(year(t),yearBuf,10);
    
    strcpy (buffer, dayShortStr(weekday(t)));
    strcat (buffer, " ");
    strcat (buffer, dayBuf);
    strcat (buffer, " ");
    strcat (buffer, monthShortStr(month(t)));
    strcat (buffer, " ");
    strcat (buffer, yearBuf);

    break;
    
   case 6:// Mon 07 Mar 2012
   //char buf[5];
    //return dayShortStr(weekday(t)) + ' ' + zeroPad(day(t)) + ' ' + monthShortStr(month(t)) + ' ' + itoa(year(t),buf,10);
    
    zeroPad(day(t), dayBuf);
    itoa(year(t),yearBuf,10);
    
    strcpy (buffer, dayShortStr(weekday(t)));
    strcat (buffer, " ");
    strcat (buffer, dayBuf);
    strcat (buffer, " ");
    strcat (buffer, monthShortStr(month(t)));
    strcat (buffer, " ");
    strcat (buffer, yearBuf);


    break;
    
   case 7:// 14:25 GMT
   
     zeroPad(hour(t), hourBuf);
     zeroPad(minute(t), minuteBuf);
     
     strcpy (buffer, hourBuf);
     strcat (buffer, ":");
     strcat (buffer, minuteBuf);

    break;
    
  }
  return buffer;
  
}





void demonstration()
{
  sayIt("Hi. My name is Blue reef.", true);
  sayIt("I am the worlds first talking interactive aquarium.", true);
  
}





char* getIpAddress(){
  return ipAddress;
}
void setIpAddress(char* ip){
  strcpy(ipAddress, ip);
}







void fadeLed(){
  if(getLightsAreOn()){
    if(ledBrighter){
      ledBrightness += 5;
      if(ledBrightness == 162){
        ledBrighter =  false;
      }
    }else{
      ledBrightness -= 5;
      if(ledBrightness == 2){
        ledBrighter = true;
      }
    }
  }else{
    ledBrightness = 0;
  }
  analogWrite(ledPin, ledBrightness);
}


void flashLed(byte times){
  
  for(byte i=0;i<times;i++){
    
    digitalWrite(ledPin, HIGH);
    delay(200);
    
    digitalWrite(ledPin, LOW);
    delay(200);
  }
}

/*
void flashLed(byte times = 1){
  
  for(byte i=0;i<times;i++){
    
    digitalWrite(ledPin, HIGH);
    delay(200);
    
    digitalWrite(ledPin, LOW);
    delay(200);
  }
}*/

long microsecondsToInches(long microseconds)
{
  // According to Parallax's datasheet for the PING))), there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second).  This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}

