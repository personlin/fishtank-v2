#include <Arduino.h>
#include <EEPROM.h>

#line 1 "src/Eepromy.ino"
//#include <EEPROM.h>

// ################# External global variables #################
extern int8_t matrixFullBrightness;
extern byte lightLevel;
extern uint8_t timeOnHour;
extern uint8_t timeOnMinute;
extern uint8_t timeOffHour;
extern uint8_t timeOffMinute;
extern uint8_t autoFeedEnabled;
extern uint8_t autoFeedHour;
extern uint8_t autoFeedMinute;





// EEPROM addresses
const byte lightLevelAddr = 0;
const byte matrixFullBrightnessAddr = 1;
const byte timeOnHourAddr = 2;
const byte timeOnMinuteAddr = 3;
const byte timeOffHourAddr = 4;
const byte timeOffMinuteAddr = 5;
const byte autoFeedEnabledAddr = 6;
const byte autoFeedHourAddr = 7;
const byte autoFeedMinuteAddr = 8;
const byte voiceVolumeAddr = 8;
const byte eventlogPointerAddr = 99;





void initEeprom()
{
  matrixFullBrightness = EEPROM.read(matrixFullBrightnessAddr);

   
  //autoFeedEnabled = EEPROM.read(autoFeedEnabledAddr);
  //autoFeedHour = EEPROM.read(autoFeedHourAddr);
  //autoFeedMinute = EEPROM.read(autoFeedMinuteAddr);
  
  //voiceVolume = EEPROM.read(voiceVolumeAddr);
}



void eepromWrite(uint8_t addr, uint8_t val)
{
  EEPROM.write(addr, val);
}

int8_t eepromRead(uint8_t addr)
{
  uint8_t result = EEPROM.read(addr);
  return result;
}



// Voice volume
void eepromSaveVolume(int8_t val)
{
  eepromWrite(voiceVolumeAddr, val);
}
int8_t eepromLoadVolume()
{
  return eepromRead(voiceVolumeAddr);
}

// Light level
void eepromSaveLightLevel(int8_t val)
{
  eepromWrite(lightLevelAddr, val);
}
int8_t eepromLoadLightLevel()
{
  return eepromRead(lightLevelAddr);
}

// Matrix brightness
void eepromSaveMatrixMaxBrightness(int8_t val)
{
  eepromWrite(matrixFullBrightnessAddr, val);
}
int8_t eepromLoadMatrixMaxBrightness()
{
  return eepromRead(matrixFullBrightnessAddr);
}

// Logger Pointer
void eepromSaveLoggerPointer(int8_t val)
{
  eepromWrite(eventlogPointerAddr, val);
}
int8_t eepromLoadLoggerPointer()
{
  return eepromRead(eventlogPointerAddr);
}

void eepromSaveAutofeedEnabled(boolean val)
{
  eepromWrite(autoFeedEnabledAddr, val?1:0);
}
int8_t eepromLoadAutofeedEnabled()
{
  return eepromRead(autoFeedEnabledAddr);
}

void eepromSaveAutofeedTime(int8_t h, int8_t m)
{
  eepromWrite(autoFeedHourAddr, h);
  eepromWrite(autoFeedMinuteAddr, m);
}
void eepromLoadAutofeedTime(uint8_t* h, uint8_t* m)
{
  *h = eepromRead(autoFeedHourAddr);
  *m = eepromRead(autoFeedMinuteAddr);
}

void eepromSaveLightsOnTime(uint8_t h, uint8_t m)
{
  eepromWrite(timeOnHourAddr, h);
  eepromWrite(timeOnMinuteAddr, m);
}
void eepromLoadLightsOnTime(uint8_t* h, uint8_t* m)
{
  *h = eepromRead(timeOnHourAddr);
  *m = eepromRead(timeOnMinuteAddr);
}

void eepromSaveLightsOffTime(uint8_t h, uint8_t m)
{
  eepromWrite(timeOffHourAddr, h);
  eepromWrite(timeOffMinuteAddr, m);
}
void eepromLoadLightsOffTime(uint8_t* h, uint8_t* m)
{
  *h = eepromRead(timeOffHourAddr);
  *m = eepromRead(timeOffMinuteAddr);
}

