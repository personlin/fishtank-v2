#ifndef _Serial_h
#define _Serial_h

void initSerial();
void attachCommandCallbacks();
void serialFunctionShowhelp();
void OnUnknownCommand();
void serialArduinoReady();
void serialFunctionLightlevel();
void serialFunctionScreenlevel();
void serialFunctionLightontime();
void serialFunctionLightofftime();
void serialFunctionServoup();
void serialFunctionServodown();
void serialFunctionFeed();
void serialFunctionFeedenable();
void serialFunctionFeeddisable();
void serialFunctionMessagematrix();
void serialFunctionMessagevoice();
void serialFunctionVoicevolume();
void serialFunctionSetipaddress();
void serialFunctionSetmultiple();
void serialFunctionSettime();
void serialFunctionGetdata();
void serialHandleData();
void serialFunctionGettime();
void requestTimeFromSerial();

#endif