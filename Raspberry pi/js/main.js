    var tid; // Timer ID for AJAX load timer
	var saj;// AJAX id for save call
	var laj;
	
	var lightLevel;
	var screenLevel;

	$(document).delegate('#settings', 'pageinit', function(){ 
		
		$( "#settings form .submit").on('click', function(e){
			e.preventDefault();

			//$.mobile.changePage($('#main'), {

			//}); 
			var settings = [$('#lights-on').val(), $('#lights-off').val(), $('#feedTime').val(), $('#chkFeed').val()];
			ajaxSave(arduinoFunctions.Setmultiple, settings);


			// ajaxSave('<?php echo arduinoFunctions::Setlightontime; ?>', $('#lights-on').val());
			// ajaxSave('<?php echo arduinoFunctions::Setlightofftime; ?>', $('#lights-off').val());
			// ajaxSave('<?php echo arduinoFunctions::Setlightlevel; ?>', $('#sldLights').val());
			// ajaxSave('<?php echo arduinoFunctions::Setscreenbrightness; ?>', $('#sldScreen').val());
			// ajaxSave('<?php echo arduinoFunctions::Setvoicevolume; ?>', $('#sldVolume').val());

			

			$('#settings').dialog('close');

			//return false;
		});
		
		$('#pump-down').on('click', function(e){
			ajaxSave(arduinoFunctions.Servodown, '1');
		});
		$('#pump-up').on('click', function(e){
			ajaxSave(arduinoFunctions.Servoup, '1');
		});
		
	    $('#sldLights').on('change', function() {
		  ajaxSave(arduinoFunctions.Setlightlevel, $(this).val());
	    });
		
	    $('#sldScreen').on('change', function() {
		  ajaxSave(arduinoFunctions.Setscreenbrightness, $(this).val());
	    });

	    $('#sldVolume').on('change', function() {
		  ajaxSave(arduinoFunctions.Setvoicevolume, $(this).val());
	    });

		$('#feed').on('click', function(e){
			ajaxSave(arduinoFunctions.Triggerfeeder, '1');
		});
		
	});
	$(document).delegate('#settings', 'pagebeforeshow', function(){

		clearTimeout(tid);
		
		if(laj != null)laj.abort();
	});
	$(document).delegate('#settings', 'pageshow', function(){

		$('#sldLights').val(lightLevel).slider("refresh"); 
		$('#sldScreen').val(screenLevel).slider("refresh");

	});
	
	
	$(document).delegate('#message', 'pageinit', function(){



		$( "input[name='type']" ).on('change', function(e){
			var btnText = $(this).next().text() + '!';
			//alert(btnText);
			$('.ui-submit span').text(btnText);
		});
		$( "#message .auto" ).on('click', function(e){
			e.preventDefault();
			$('#msg').insertAtCaret( $(this).text());
		});
		
		$( "#message form .submit" ).on('click', function(e){
			e.preventDefault();
			ajaxSave(($("#message input[name='type']").val() == 'speak'?'speakmsg':'showmsg'), $("#message input[name='msg']").val());
			$('#message').dialog('close');
			//return false;
		});
	});
	$(document).delegate('#message', 'pagebeforeshow', function(){
		clearTimeout(tid);
		$('#msg').val('');
		if(laj != null)laj.abort();

		// Show/speak default value
		$("input[name='type']:first").attr("checked",true).checkboxradio("refresh");
		$('.ui-submit span').text('Show it!');
	});
	
	$('#main').on('pageinit', function(){ 

    
	});
	
	$(document).delegate('#main', 'pagebeforeshow', function(){
		clearTimeout(tid);
		ajaxLoad();
		
		//tid = setTimeout(ajaxLoad, 5000);
		
	});
	
	$(document).delegate('#password-page', 'pageinit', function(){ 

	  $('#password-page form').submit(function(e){
	  	e.preventDefault();

	  	if($("#password-page input[type='password']").val() == myPassword){
	  		$('#password-page').dialog('close');
	  		
		    setTimeout(function () {
		     $.mobile.changePage($('#settings'), { transition: "pop", role: "dialog"});
		  }, 100);
	  	}else{

	  		$('#password-page').dialog('close');
	  	}
	  	return false;
	  });
    	
    
	});

    function ajaxLoad(){
		clearTimeout(tid);
		/*
		laj = $.post('<?php echo $_SERVER["SCRIPT_NAME"];?>', {action: 'load'}, function(data) {
		*/
		
		laj = $.ajax({
			type: "POST",
			url: window.location.pathname,
			data: {action: "load"},
			dataType: "json",
			/*error: function (XMLHttpRequest, textStatus, errorThrown) {
				alert("XMLHttpRequest: " + XMLHttpRequest);
				alert("textStatus: " + textStatus);
				alert("errorThrown: " + errorThrown);
			},*/
			success: function(data){ 
				//alert('a');
				//alert(data['temp']);
				console.log(data);
				if(data == null){
					//alert('IS FUCKING NULL!');
				}else{
				//alert('data:' + data['temp']);
				  $('#tempContainer .value').html(data['temp']);
				  $('#levelContainer .value').html(data['waterlevel']);
				  $('#lidContainer .value').html(data['lidopen']=='1'?'Open':'Closed');
				  $('#timeContainer .value').html(data['time']);
				  $('#dateContainer .value').html(data['date']);
				  $('#timeOnContainer .value').html(parseInt(data['tonh']).leftZeroPad(2) + ':' + parseInt(data['tonm']).leftZeroPad(2));
				  $('#timeOffContainer .value').html(parseInt(data['tofh']).leftZeroPad(2) + ':' + parseInt(data['tofm']).leftZeroPad(2));
				  $('#lightonContainer .value').html(data['lighton']=='1'?'On':'Off');
				  
				  if($('#events').html().length == 0 && data.length){
					  $.each(data['events'], function(key, value) {
						
						switch(value[5]){
							case 0:
								var cls = 'alert';
								var event = 'Low Temperature';
							break;
							case 1:
								var cls = 'alert';
								var event = 'High Temperature';
							break;
							case 2:
								var cls = 'alert';
								var event = 'Low Water';
							break;
							case 3:
								var cls = 'alert';
								var event = 'Leak Detected';
							break;
							case 4:
								var cls = 'info';
								var event = 'Started Up';
							break;
							case 5:
								var cls = 'info';
								var event = 'Manual Feed';
							break;
							case 5:
								var cls = 'info';
								var event = 'Auto Feed';
							break;
						}
						//alert(event);
						
							$('#events').prepend('<div class="event"><span>['+ value[2] + '/' + value[1]  + '/' + value[0] + ' - ' + value[3] + ':' + value[4] +']</span> '+ event +'</div>');
					  });
				  }
				  
				  /* Settings page */
				  var onTime = (parseInt(data['tonh']) * 60) + parseInt(data['tonm']);
				  $('#lights-on').val(onTime);
				  var offTime = (parseInt(data['tofh']) * 60) + parseInt(data['tofm']);
				  $('#lights-off').val(offTime);
				  
				  /*
				  $('#sldLights').die('change');
				  $('#sldLights').val(data['light']);
				  $('#sldLights').on('change', function() {
					  //alert('e');ajaxSave();
					  ajaxSave('lights');
				  });*/
				  
				  lightLevel = data['light'];
				  screenLevel = data['screen'];
				  
				  
				  
				}
				tid = setTimeout(ajaxLoad, 5000);
			}
		});
      
	}
    
      function ajaxSave(sFunc, sVal){
		   if(saj != null)saj.abort();
		   if(laj != null)laj.abort();
		   
		   clearTimeout(tid);
		   

			//var settings = {action: 'save', func: 'settings', lon: $('#lights-on').val(), loff: $('#lights-off').val(), chkFeed: $('#chkFeed').val()};

			var settings = { action: 'save', func: sFunc, val: sVal };

		  saj = $.post(window.location.pathname, settings, function(data){
			
			if(data['alert'] != undefined)alert(data['alert']);

		  });
    	  
		  
      }

	  
	  
	  
	Number.prototype.leftZeroPad = function(numZeros) {
		var n = Math.abs(this);
		var zeros = Math.max(0, numZeros - Math.floor(n).toString().length );
		var zeroString = Math.pow(10,zeros).toString().substr(1);
		if( this < 0 ) {
			zeroString = '-' + zeroString;
		}
	
		return zeroString+n;
	}
	
	$.fn.extend({
		insertAtCaret: function(myValue){
		var obj;
		if( typeof this[0].name !='undefined' ) obj = this[0];
		else obj = this;
	      
		if ($.browser.msie) {
		  obj.focus();
		  sel = document.selection.createRange();
		  sel.text = myValue;
		  obj.focus();
		  }
		else if ($.browser.mozilla || $.browser.webkit) {
		  var startPos = obj.selectionStart;
		  var endPos = obj.selectionEnd;
		  var scrollTop = obj.scrollTop;
		  obj.value = obj.value.substring(0, startPos)+myValue+obj.value.substring(endPos,obj.value.length);
		  obj.focus();
		  obj.selectionStart = startPos + myValue.length;
		  obj.selectionEnd = startPos + myValue.length;
		  obj.scrollTop = scrollTop;
		} else {
		  obj.value += myValue;
		  obj.focus();
		 }
	       }
      });

