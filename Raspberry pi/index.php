<?php
/*
 * Fishtank interface
 * Last modified: 26/11/2012
 * - 2/10/2012: Added show/speak option
 * - 26/11/2012: Added feed fish button, auto/manual feed events, password popup, POST return alert messages
 * - 
 */

//$fishtankIp 		= '192.168.0.98';
$ipWhitelist 		= array('78.32.170.70');
$enableIpWhiteList 	= false;
$password 			= 'maureen';// Required to edit fishtank settings
$serialPortIp		= '';
$serialPortPort		= '8080';
// ---------------------------------------------------------------------------------

class arduinoFunctions
{
	const Showhelp 				= 0;
	const Setlightlevel 		= 1;
	const Setscreenbrightness	= 2;
	const Setlightontime		= 3;
	const Setlightsofftime		= 4;
	const Servoup				= 5;
	const Servodown				= 6;
	const Triggerfeeder			= 7;
	const Enableautofeeder		= 8;
	const Disableautofeeder		= 9;
	const Displaymatrixmessage	= 10;
	const Speakmessage			= 11;
	const Setvoicevolume		= 12;
	const Setipaddress			= 13;
	const Setmultiple			= 14;
	const Sendbackalldata		= 15;
}



if($enableIpWhiteList && !in_array($_SERVER['REMOTE_ADDR'], $ipWhitelist))die("Access disallowed due to IP restriction.");


// Send rav data to serial
function serialSendData($data)
{
die('A: ' . $data);
	if ($socket=socket_create(AF_INET, SOCK_STREAM, SOL_TCP) and
	  (socket_connect($socket, $serialPortIp, $serialPortPort)))
	{
	  $error="Connection successful on IP $address, port $port";
	  
	}
	else
	$error="Unable to connect<pre>".socket_strerror(socket_last_error())."</pre>";

}

// Send 
function serialSend($function, $params){

	$data = $function;
	if(is_array($params)) $data .= ',' . implode(',', $params);
	elseif(!is_null($params))$data .= ',' . $params;

	$data .= ';';

	return serialSendData($data);
}




//die(json_encode(array('a' => '1', 'b' => '2')));
//die(json_encode(array('test' => 123, 'arr' => array(array('1', 'b'),array('1', 'b')))));

if($_SERVER['REQUEST_METHOD']=='POST'){

	if(!isset($_POST['action']))die('error: no action POSTed');

/*
0: Show Help
1: Set light level
2: Set screen brightness
3: Set light on time
4: Set lights off time
5: Servo up
6: Servo down
7: Trigger feeder
8: Enable auto-feeder
9: Disable auto-feeder
10: Display matrix message
11: Speak message
12: Set voice volume
13: Set IP address#
14: Set multiple settings at once
15: Set time (timestamp)
16: Send back all data
17: Get current time
*/




	switch($_POST['action']){
		case 'load':

			// TODO: Grab info from arduino via serial

			$json = '';
			
			$data = array(
				'waterlevel' =>'80',
				'leak' => '0',
				'lidopen' => '0',
				'temp' => '23',
				'light' => '100',
				'pump' => '66',
				'time' => '17:45',
				'date' => '01/06/2012',
				'tonh' => '04',
				'tonm' => '50',
				'tofh' => '12',
				'tofm' => '34'
			);
			header('Content-type: application/json');
			die(json_encode($data));
			die($json);
		break;
		case 'save':
			$fields_string  = '';
			

			$result = serialSend($_POST['func'], $_POST['val']);
			//header();
			die($result);
		break;
	}
	curl_close($ch);

//exit;
}
?><!DOCTYPE html> 
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/> <!--320-->
	<title>Blueleaf Aquarium</title> 
	<link rel="stylesheet" href="css/jquery.mobile-1.3.2.min.css" />
	<link rel="stylesheet" href="css/main.css">
	<script src="js/jquery-1.9.1.js"></script>
	<script src="js/jquery.mobile-1.3.2.min.js"></script>
	
	
	<script type="text/javascript">
		var myPassword = "<?php echo $password; ?>";

		var arduinoFunctions = {
			Setlightontime: '<?php echo arduinoFunctions::Setlightontime; ?>',
			Showhelp: '<?php echo arduinoFunctions::Showhelp; ?>',
			Setlightlevel: '<?php echo arduinoFunctions::Setlightlevel; ?>',
			Setscreenbrightness: '<?php echo arduinoFunctions::Setscreenbrightness; ?>',
			Setlightontime: '<?php echo arduinoFunctions::Setlightontime; ?>',
			Setlightsofftime: '<?php echo arduinoFunctions::Setlightsofftime; ?>',
			Servoup: '<?php echo arduinoFunctions::Servoup; ?>',
			Servodown: '<?php echo arduinoFunctions::Servodown; ?>',
			Triggerfeeder: '<?php echo arduinoFunctions::Triggerfeeder; ?>',
			Enableautofeeder: '<?php echo arduinoFunctions::Enableautofeeder; ?>',
			Disableautofeeder: '<?php echo arduinoFunctions::Disableautofeeder; ?>',
			Displaymatrixmessage: '<?php echo arduinoFunctions::Displaymatrixmessage; ?>',
			Speakmessage: '<?php echo arduinoFunctions::Speakmessage; ?>',
			Setvoicevolume: '<?php echo arduinoFunctions::Setvoicevolume; ?>',
			Setipaddress: '<?php echo arduinoFunctions::Setipaddress; ?>',
			Setmultiple: '<?php echo arduinoFunctions::Setmultiple; ?>',
			Sendbackalldata: '<?php echo arduinoFunctions::Sendbackalldata; ?>',

		};

	</script>
  
</head> 
<body>
<div data-role="page" id="main">
	<div data-role="header">
		<h1>Blueleaf Aquarium</h1>
	</div><!-- /header -->
	<div data-role="content">
	
		<fieldset class="ui-grid-a">
			<div class="ui-block ui-block-a">
				<div class="block-header">Water Temp</div>
				<div class="block-content">
					<div id="tempContainer"><span class="value">0</span>&deg;C</div>
				</div>
			</div>
			<div class="ui-block ui-block-b">
				<div class="block-header">Water Level</div>
				<div class="block-content">
					<div id="levelContainer"><span class="value">0</span>%</div>
				</div>
			</div>
		</fieldset>
		<fieldset class="ui-grid-a">
			<div class="ui-block ui-block-a">
				<div class="block-header">Lights</div>
				<div class="block-content">
					<div id="lightonContainer"><span class="value">Off</span></div>
					<div id="timeOnContainer">Lights On: <span class="value">00:00</span></div>
					<div id="timeOffContainer">Lights Off: <span class="value">00:00</span></div>
				</div>
			</div>
			<div class="ui-block ui-block-b">
				<div class="block-header">Lid</div>
				<div class="block-content">
					<div id="lidContainer"><span class="value">Closed</span></div>
				</div>
			</div>
		
		</fieldset>
		<fieldset class="ui-grid-a">
		
			<div class="ui-block ui-block-a">
				<div class="block-header">Time/Date</div>
				<div class="block-content">
					<div id="timeContainer"><span class="value">-</span></div>
					<div id="dateContainer"><span class="value">-</span></div>
				</div>
			</div>
		
			<div class="ui-block ui-block-b">
				<div class="block-header">Event Log</div>
				<div class="block-content">
					<div id="events"></div>
				</div>
			</div>

		</fieldset>
		


	
		<p><a href="#password-page" data-role="button" data-rel="dialog" data-transition="pop">Settings</a><br />
        <a href="#message" data-role="button" data-rel="dialog" data-transition="pop">Show/Speak Message</a></p>
	</div>
	
	<div data-role="footer" data-theme="d">
		<h4>Blueleaf</h4>
	</div>
</div>

<div data-role="page" id="settings">
	<div data-role="header" data-theme="e">
		<h1>Settings</h1>
	</div>
	<div data-role="content" data-theme="d">
            <form id="settings-form" method="post" action="<?php echo $_SERVER["SCRIPT_NAME"];?>">
				<input type="hidden" name="action" value="save" />
				<fieldset>
                    <h2>Lights</h2>
                       <label for="lights-on">Lights On</label>	
                        <select name="lon" id="lights-on">
                            <option value="0">00:00</option><option value="15">00:15</option><option value="30">00:30</option><option value="45">00:45</option><option value="60">01:00</option><option value="75">01:15</option><option value="90">01:30</option><option value="105">01:45</option><option value="120">02:00</option><option value="135">02:15</option><option value="150">02:30</option><option value="165">02:45</option><option value="180">03:00</option><option value="195">03:15</option><option value="210">03:30</option><option value="225">03:45</option><option value="240">04:00</option><option value="255">04:15</option><option value="270">04:30</option><option value="285">04:45</option><option value="300">05:00</option><option value="315">05:15</option><option value="330">05:30</option><option value="345">05:45</option><option value="360">06:00</option><option value="375">06:15</option><option value="390">06:30</option><option value="405">06:45</option><option value="420">07:00</option><option value="435">07:15</option><option value="450">07:30</option><option value="465">07:45</option><option value="480">08:00</option><option value="495">08:15</option><option value="510">08:30</option><option value="525">08:45</option><option value="540">09:00</option><option value="555">09:15</option><option value="570">09:30</option><option value="585">09:45</option><option value="600">10:00</option><option value="615">10:15</option><option value="630">10:30</option><option value="645">10:45</option><option value="660">11:00</option><option value="675">11:15</option><option value="690">11:30</option><option value="705">11:45</option>
							<option value="720">12:00</option><option value="735">12:15</option><option value="750">12:30</option><option value="765">12:45</option><option value="780">13:00</option><option value="795">13:15</option><option value="810">13:30</option><option value="825">13:45</option><option value="840">14:00</option><option value="855">14:15</option><option value="870">14:30</option><option value="885">14:45</option><option value="900">15:00</option><option value="915">15:15</option><option value="930">15:30</option><option value="945">15:45</option><option value="960">16:00</option><option value="975">16:15</option><option value="990">16:30</option><option value="1005">16:45</option><option value="1020">17:00</option><option value="1035">17:15</option><option value="1050">17:30</option><option value="1065">17:45</option><option value="1080">18:00</option><option value="1095">18:15</option><option value="1110">18:30</option><option value="1125">18:45</option><option value="1140">19:00</option><option value="1155">19:15</option><option value="1170">19:30</option><option value="1185">19:45</option><option value="1200">20:00</option><option value="1215">20:15</option><option value="1230">20:30</option><option value="1245">20:45</option><option value="1260">21:00</option><option value="1275">21:15</option><option value="1290">21:30</option><option value="1305">21:45</option><option value="1320">22:00</option><option value="1335">22:15</option><option value="1350">22:30</option><option value="1365">22:45</option><option value="1380">23:00</option><option value="1395">23:15</option><option value="1410">23:30</option><option value="1425">23:45</option>
                        
						</select>
                       <label for="lights-off">Lights Off</label>	
                        <select name="loff" id="lights-off">
                            <option value="0">00:00</option><option value="15">00:15</option><option value="30">00:30</option><option value="45">00:45</option><option value="60">01:00</option><option value="75">01:15</option><option value="90">01:30</option><option value="105">01:45</option><option value="120">02:00</option><option value="135">02:15</option><option value="150">02:30</option><option value="165">02:45</option><option value="180">03:00</option><option value="195">03:15</option><option value="210">03:30</option><option value="225">03:45</option><option value="240">04:00</option><option value="255">04:15</option><option value="270">04:30</option><option value="285">04:45</option><option value="300">05:00</option><option value="315">05:15</option><option value="330">05:30</option><option value="345">05:45</option><option value="360">06:00</option><option value="375">06:15</option><option value="390">06:30</option><option value="405">06:45</option><option value="420">07:00</option><option value="435">07:15</option><option value="450">07:30</option><option value="465">07:45</option><option value="480">08:00</option><option value="495">08:15</option><option value="510">08:30</option><option value="525">08:45</option><option value="540">09:00</option><option value="555">09:15</option><option value="570">09:30</option><option value="585">09:45</option><option value="600">10:00</option><option value="615">10:15</option><option value="630">10:30</option><option value="645">10:45</option><option value="660">11:00</option><option value="675">11:15</option><option value="690">11:30</option><option value="705">11:45</option>
							<option value="720">12:00</option><option value="735">12:15</option><option value="750">12:30</option><option value="765">12:45</option><option value="780">13:00</option><option value="795">13:15</option><option value="810">13:30</option><option value="825">13:45</option><option value="840">14:00</option><option value="855">14:15</option><option value="870">14:30</option><option value="885">14:45</option><option value="900">15:00</option><option value="915">15:15</option><option value="930">15:30</option><option value="945">15:45</option><option value="960">16:00</option><option value="975">16:15</option><option value="990">16:30</option><option value="1005">16:45</option><option value="1020">17:00</option><option value="1035">17:15</option><option value="1050">17:30</option><option value="1065">17:45</option><option value="1080">18:00</option><option value="1095">18:15</option><option value="1110">18:30</option><option value="1125">18:45</option><option value="1140">19:00</option><option value="1155">19:15</option><option value="1170">19:30</option><option value="1185">19:45</option><option value="1200">20:00</option><option value="1215">20:15</option><option value="1230">20:30</option><option value="1245">20:45</option><option value="1260">21:00</option><option value="1275">21:15</option><option value="1290">21:30</option><option value="1305">21:45</option><option value="1320">22:00</option><option value="1335">22:15</option><option value="1350">22:30</option><option value="1365">22:45</option><option value="1380">23:00</option><option value="1395">23:15</option><option value="1410">23:30</option><option value="1425">23:45</option>
                        
						</select>
						
						
						<label for="sldLights">Light level:</label>
						<input type="range" name="sldLights" id="sldLights" value="0" min="0" max="255" data-highlight="true" />
						
						<label for="sldScreen">Screen Brightness:</label>
						<input type="range" name="sldScreen" id="sldScreen" value="1" min="1" max="16" data-highlight="true" />
						
						<label for="sldVolume">Voice Volume:</label>
						<input type="range" name="sldVolume" id="sldVolume" value="1" min="1" max="67" data-highlight="true" />


						<h2>Feeding</h2>
						<label><input type="checkbox" name="chkFeed" id="chkFeed" value="1" /> <span>Automatically feed my fish daily</span></label>

						<label><span>Feed time:</span>
                        <select name="feedTime" id="feedTime">
                            <option value="0">00:00</option><option value="15">00:15</option><option value="30">00:30</option><option value="45">00:45</option><option value="60">01:00</option><option value="75">01:15</option><option value="90">01:30</option><option value="105">01:45</option><option value="120">02:00</option><option value="135">02:15</option><option value="150">02:30</option><option value="165">02:45</option><option value="180">03:00</option><option value="195">03:15</option><option value="210">03:30</option><option value="225">03:45</option><option value="240">04:00</option><option value="255">04:15</option><option value="270">04:30</option><option value="285">04:45</option><option value="300">05:00</option><option value="315">05:15</option><option value="330">05:30</option><option value="345">05:45</option><option value="360">06:00</option><option value="375">06:15</option><option value="390">06:30</option><option value="405">06:45</option><option value="420">07:00</option><option value="435">07:15</option><option value="450">07:30</option><option value="465">07:45</option><option value="480">08:00</option><option value="495">08:15</option><option value="510">08:30</option><option value="525">08:45</option><option value="540">09:00</option><option value="555">09:15</option><option value="570">09:30</option><option value="585">09:45</option><option value="600">10:00</option><option value="615">10:15</option><option value="630">10:30</option><option value="645">10:45</option><option value="660">11:00</option><option value="675">11:15</option><option value="690">11:30</option><option value="705">11:45</option>
							<option value="720">12:00</option><option value="735">12:15</option><option value="750">12:30</option><option value="765">12:45</option><option value="780">13:00</option><option value="795">13:15</option><option value="810">13:30</option><option value="825">13:45</option><option value="840">14:00</option><option value="855">14:15</option><option value="870">14:30</option><option value="885">14:45</option><option value="900">15:00</option><option value="915">15:15</option><option value="930">15:30</option><option value="945">15:45</option><option value="960">16:00</option><option value="975">16:15</option><option value="990">16:30</option><option value="1005">16:45</option><option value="1020">17:00</option><option value="1035">17:15</option><option value="1050">17:30</option><option value="1065">17:45</option><option value="1080">18:00</option><option value="1095">18:15</option><option value="1110">18:30</option><option value="1125">18:45</option><option value="1140">19:00</option><option value="1155">19:15</option><option value="1170">19:30</option><option value="1185">19:45</option><option value="1200">20:00</option><option value="1215">20:15</option><option value="1230">20:30</option><option value="1245">20:45</option><option value="1260">21:00</option><option value="1275">21:15</option><option value="1290">21:30</option><option value="1305">21:45</option><option value="1320">22:00</option><option value="1335">22:15</option><option value="1350">22:30</option><option value="1365">22:45</option><option value="1380">23:00</option><option value="1395">23:15</option><option value="1410">23:30</option><option value="1425">23:45</option>
						</select>
						</label>

						<h2>Pump</h2>
						<div class="ui-grid-a">
						<div class="ui-block-a"><input id="pump-down" name="pump-down" type="button" value="< Bubbles" /></div>
						<div class="ui-block-b"><input id="pump-up" name="pump-up" type="button" value="Filter >" /></div>
						</div><!-- /grid-a -->
						
						<input type="button"  id="feed" name="feed"  value="Feed the fish" />
						<br />
                    <input type="submit" data-theme="b" name="submit" class="submit" value="Save" />
				</fieldset>
            </form>
                	
	</div>

</div>

<div data-role="page" id="message">
	<div data-role="header" data-theme="e">
		<h1>Matrix Message</h1>
	</div>
	<div data-role="content" data-theme="d">
        <form method="post" action="<?php echo $_SERVER["SCRIPT_NAME"];?>">
			<input type="hidden" name="action" value="save" />
		<fieldset>
			<label for="msg">Insert:</label>
			<div id="auto-options">
				<a href="#" class="auto">Hello</a>
				<a href="#" class="auto">Welcome</a>
				<a href="#" class="auto">Welcome to Blueleaf</a>
				<a href="#" class="auto">Blueleaf</a>
				<a href="#" class="auto">Good morning</a>
				<a href="#" class="auto">Good afternoon</a>
				<a href="#" class="auto">The way the fishtank should be.</a>
				<a href="#" class="auto">I"m watching you...</a>
                                <a href="#" class="auto">Bye</a>
                                <a href="#" class="auto">Goodbye</a>
                                <a href="#" class="auto">See you soon</a>
				<a href="#" class="auto">Go away!</a>
				<div style="clear: both;"></div>
			</div>
			
                  <label for="msg">Your message:</label>
				  <input type="text" name="msg" id="msg" value="" maxlength="64"  />
		</fieldset>

		<fieldset data-theme="b" data-role="controlgroup" data-type="horizontal" style="text-align: center;">
                  <input type="radio" name="type" id="type-show" value="show" checked="checked" /><label for="type-show">Show it</label>
                  <input type="radio" name="type" id="type-speak" value="speak" /><label for="type-speak">Speak it</label>
        </fieldset>

                  
                  <button type="submit" data-theme="b" name="submit" class="submit" id="btn-submit" value="submit-value">Show it!</button>
		
               </form>
	</div>
	
	<div data-role="footer">
		<h4>Blueleaf</h4>
	</div>
</div>


<div data-role="page" id="password-page">
	<div data-role="header" data-theme="e">
		<h1>Enter password</h1>
	</div>
	<div data-role="content" data-theme="d">
        <form method="post" action="" data-ajax="false">
        	<input type="password" name="password" />
        	<button type="submit" data-theme="b" name="submit" class="submit" id="btn-submit" value="submit-value">Authenticate</button>
		
        </form>
    </div>
</div>
<script src="js/main.js" type="text/javascript"> </script>
</body>
</html>