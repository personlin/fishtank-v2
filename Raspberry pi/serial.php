#!/usr/bin/php
<?php
	set_time_limit(0);
	ob_implicit_flush(); 

	include('php_serial.class.php');

	define('HTTP_PORT', '8080');

	function echof($data){
		file_put_contents('debug.txt', $data,  FILE_APPEND );
	}

	function getIp()
	{
		exec('/sbin/ifconfig', $resultArray);
		$result = implode(",", $resultArray);
		
		$ip = preg_match('/eth0.*?inet\saddr:([\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})\s/', $result, $matches);

		return isset($matches[1])?$matches[1]:false;
	}

	function sendSerial($func, $params = array())
	{
		global $serial;
		if(is_array($params)){
			$paramStr = implode(',', $params);
		}else{
			$paramStr = $params;
		}
		$send = $func . ($paramStr?','.$paramStr:'') . ';';
		echof("Sending to Serial: [$send]\n");
		$serial->sendMessage($func . ($paramStr?','.$paramStr:'') . ';');
		usleep(10000);// 10ms
	}

	function processSerialData($data)
	{echof("From Serial: [$data]\n");
		$commands = explode(';', $data);
		foreach($commands as $command){
			//echo "Serial command: " . $command;
			$parts = explode(',', $data);
			$params = array_slice($parts, 1);
			processSerialCommand($parts[0], $params);
		}
		
	}

	function processSerialCommand($func, $params = array())
	{ 
		global $sock;
		switch($func){
			case '21':// Get time
			//echo "Sending time.\n";
				sendSerial(15,time());
			break;
			default:
				$output = $func . ',' . implode(',', $params);
				echof("Sending to Socket: [" . $output . "]\n");
				socket_write($sock, $output, strlen($output));
			break;
		}
		usleep(10000);// 10ms
	}


	function processSocketData($data)
	{echof("From Socket: [$data]\n");
		global $serial, $sock;
		$data = str_replace(array("\r", "\n"), '', $data);
		
		if($data === '')return;

		if(strpos($data, 'exit') !== false){
		$serial->deviceClose(); 
		socket_close($sock);
		exit;
		}

		$commands = explode(';', $data);
		foreach($commands as $command){
			if($command === '')continue;
			//echo "\nData: " . $command . "\n";

			if(strpos($command, ',') !== false){
				$parts = explode(',', $command);
				//echo "\nParts: [" . implode(',',$parts) . "]\n";
				$params = array_slice($parts, 1);
				$func = $parts[0];
			}else{
				$func = $command;
				$params = null;
			}
			sendSerial($func, $params);
		}
	}







	// ##### Serial Setup #####
	$serial = new phpSerial;
	$serial->deviceSet("/dev/ttyACM0");// /dev/ttyS0 | COM9 | tty.* or cu.* | "/dev/cu.usbserial-FTDY7ID6"
	$serial->confBaudRate(9600); 
	$serial->deviceOpen();


	sleep(3);

	// Send the time
	sendSerial(15,time());
	// Send our IP
	sendSerial(13, getIp());


	// ##### Socket Setup #####
	if($sock = socket_create_listen(HTTP_PORT)) < 0){
	
		 echo "socket_create_listen() failed: ". socket_strerror($sock) ."\n";
		 $serial->deviceClose();
		 exit;
	}


	$running = true;
	$serialData = '';
	while($running){
		echof('a');
		// Compile serial data, process when we find a ';'
		$buf = $serial->readPort();
		if (!empty($buf))$serialData .= $buf;
		if(strpos($serialData, ';') !== false){
			processSerialData($serialData);
			$serialData = '';
		}
echof('b');
		// Handle 
        // if (false !== ($data = socket_read($sock, 2048, PHP_NORMAL_READ))) {
        // 	processSocketData($data);
        // }
        if($client = socket_accept($sock)) {
        //$client = socket_accept($sock);
echof('c');
		    if($data=@socket_read($client, 2048, PHP_NORMAL_READ)){
echof('d');		    	
		    echo "Data: [$data]\n";
		    	processSocketData($data);
		    }
		    /*if(socket_last_error($sock) == 104) {
			    echo "Connection closed";
			    socket_close($sock);
			    break;
			}*/
echof('e');			
		}
		echo '.';
	}


	//$read = $serial->readPort();

	$serial->deviceClose(); 
	socket_close($sock);




fwrite(STDOUT, 'Fishtank serial interface started.' . $_SERVER['SERVER_ADDR']);