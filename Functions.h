#ifndef _Functions_h
#define _Functions_h

#include <Time.h>

void initPins();
void initTime();
time_t getTime();
time_t getCachedTime();
void setCachedTime(unsigned long timestamp);

int getWaterLevel();
int readTemperature();
void pumpUp();
void pumpDown();
void debug(char * message);
char* getDateTimeString(int format, char* buffer);
char* zeroPad(int val, char* buffer);

void demonstration();

char* getIpAddress();
void setIpAddress(char* ip);
void fadeLed();
void flashLed(byte times);
long microsecondsToInches(long microseconds);
long microsecondsToCentimeters(long microseconds);

#endif