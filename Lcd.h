#ifndef _Lcd_h
#define _Lcd_h

void initLcd();


void lcdSetBrightness(int8_t brightness);
void lcdDisplay(char* message, int8_t line);
void checkLcdButton();
void lcd_show_info();
void lcdOn();
void lcdOff();
void lcdPosition(int row, int col);
void lcdClear();
void lcdBacklightOn();
void lcdBacklightOff();
void lcdSerCommand();

#endif