#ifndef _Eepromy_h
#define _Eepromy_h

void initEeprom();
void eepromWrite(uint8_t addr, uint8_t val);
int8_t eepromRead(uint8_t addr);
void eepromSaveVolume(int8_t val);
int8_t eepromLoadVolume();
void eepromSaveLightLevel(int8_t val);
int8_t eepromLoadLightLevel();
void eepromSaveMatrixMaxBrightness(int8_t val);
int8_t eepromLoadMatrixMaxBrightness();
void eepromSaveLoggerPointer(int8_t val);
int8_t eepromLoadLoggerPointer();
void eepromSaveAutofeedEnabled(boolean val);
int8_t eepromLoadAutofeedEnabled();
void eepromSaveAutofeedTime(int8_t h, int8_t m);
void eepromLoadAutofeedTime(uint8_t* h, uint8_t* m);
void eepromSaveLightsOnTime(uint8_t h, uint8_t m);
void eepromLoadLightsOnTime(uint8_t* h, uint8_t* m);
void eepromSaveLightsOffTime(uint8_t h, uint8_t m);
void eepromLoadLightsOffTime(uint8_t* h, uint8_t* m);

#endif