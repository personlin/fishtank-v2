#ifndef _Lights_h
#define _Lights_h


void initLights();
void controlLights();
void setLightLevel(uint8_t val);
boolean getLightsAreOn();
void lightsOn();
void lightsOff();

#endif