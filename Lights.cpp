#include <Arduino.h>
#include <Time.h> 
#include <TimeAlarms.h>
#include "Enums.h"
#include "Globals.h"
#include "Timers.h"
#include "Lights.h"



void eepromLoadLightsOnTime(uint8_t* h, uint8_t* m);
void eepromLoadLightsOffTime(uint8_t* h, uint8_t* m);
void eepromLoadAutofeedTime(uint8_t* h, uint8_t* m);
int8_t eepromLoadLightLevel();
matrixMode getMatrixCurrentlyShowing();
void setMatrixCurrentlyShowing(matrixMode mode);
void lcdOn();
void lcdOff();
void matrixClearScreen();
void matrixWriteScreen();
void matrixSetBrightness();


// ################# External global variables #################
extern const byte lightsPin;



// ################# Global Variables #################
byte lightLevel = 255;// Brightness of LED lighting
boolean lightsAreOn = false;


void initLights(){

  
   lightLevel = eepromLoadLightLevel();
  
}



void controlLights(){

 time_t curTime = now();
 time_t onTime = AlarmHMS(getTimeOnHour(),getTimeOnMinute(),0);
 time_t offTime = AlarmHMS(getTimeOffHour(),getTimeOffMinute(),0);
 
Serial.println("Control Lights");
 
 // If we're between on/off time, turn on
 if(onTime < offTime){// Start < End
  Serial.println("on < off");
   if((curTime > previousMidnight(now()) + onTime) && (curTime < previousMidnight(now()) + offTime)){
    lightsOn();
    Serial.println("Lights are on");
  }else{
    Serial.println("Lights are off");
    lightsOff();
  }
 }else{// End > Start
   if((curTime > previousMidnight(now()) + onTime) || (curTime < previousMidnight(now()) + offTime)){
    lightsOn();
   }else{
    lightsOff();
   }
 }
  
  
  matrixSetBrightness();
  
}

void setLightLevel(uint8_t val){
  lightLevel = val;
}

boolean getLightsAreOn(){
 return lightsAreOn; 
}

void lightsOn(){
  Serial.println("Lights On Fired");
  lightsAreOn = true;

    analogWrite(lightsPin, lightLevel);
    if(getMatrixCurrentlyShowing() == matrixOff){
      setMatrixCurrentlyShowing(matrixBlank);
      //lcdSetBrightness(20);
    }
    lcdOn();
}
void lightsOff(){
  Serial.println("Lights Off Fired");
  lightsAreOn = false;
    analogWrite(lightsPin, 0);
    setMatrixCurrentlyShowing(matrixOff);

    matrixClearScreen();
    matrixWriteScreen();

    lcdOff();
}



