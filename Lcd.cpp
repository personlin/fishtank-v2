#include <Arduino.h>
#include <serLCD.h>// LCD

#include "Globals.h"
#include "Lcd.h"
#include "Functions.h"
#include "Voice.h"
#include "Timers.h"



extern const byte lcdPin;
extern const byte buttonPin;



uint8_t lcdPage = 0;


// ################# Objects #################
serLCD LCD(lcdPin);// LCD Display


void initLcd()
{
  LCD.setBrightness(20);
  delay(20);
}

void lcdSetBrightness(int8_t brightness)
{
  LCD.setBrightness(brightness);
}




void lcdDisplay(char* message, int8_t line)
{
// TODO: show text
   char buf[17];
   strcpy(buf, message);
   
   buf[16] += '\0';
   
  LCD.selectLine(line);
  LCD.print(buf);
}

void checkLcdButton(){
  
  if(digitalRead(buttonPin) == LOW){
    if(lcdPage >= 2){
      lcdPage = 0;
    }else{
      lcdPage++;
    }
    lcd_show_info();
    delay(20);
    uint8_t btnHeldTime = 0;
    while(digitalRead(buttonPin) == LOW){
      if(btnHeldTime < 250)btnHeldTime++;
      delay(10);
    }
    if(btnHeldTime == 250)sayDemonstration();// Button was held down for 2.5 secs
  }
  
  
}



void lcd_show_info(){
  
  char lineone[17];
  char linetwo[17];
      
  delay(10);
  LCD.clear();
  
  // TODO: Maybe switch below crap to sprintf
  
  switch(lcdPage){

    case 0:
      strcpy (lineone, "IP Address:");
      strcpy (linetwo, getIpAddress());
      
    break;
    case 1:

      char curtime[16];
      getDateTimeString(7, curtime);
      strcpy (lineone, curtime);
      
      char curdate[16];
      getDateTimeString(6, curdate);
      strcpy (linetwo, curdate);
      

      
    break;
    case 2:{
      int timeOnHourBst = getTimeOnHour();
      int timeOffHourBst = getTimeOffHour();
    
      //zeroPad(timeOnMinute, hourBuf)
    
      sprintf(lineone, "On:  %02d:%02d", timeOnHourBst, getTimeOnMinute());
      sprintf(linetwo, "Off: %02d:%02d", timeOffHourBst, getTimeOffMinute());
      
    break;
    }
    case 3:
    
      LCD.print("Loading...");
      
      // Temp, Depth, light detected, screen, light 
      int8_t temp = readTemperature();
      
      int8_t waterLevel = getWaterLevel();
      
      sprintf(lineone, "Temp:  %dC", temp);
      sprintf(linetwo, "Water:  %d%%", waterLevel);
      LCD.clear();// Clear loading message
    break;
  }
  
  // Lights on/Lights off
  // Current date and time
  
  
  LCD.print(lineone);
  
  LCD.selectLine(2);
  //lcdPosition(1, 1);

  LCD.print(linetwo);
  
}

// since the LCD does not send data back to the Arduino, we should only define the txPin
const int LCDdelay=20;  // conservative, 2 actually works



/*void lcdFreeMemory(){
  delay(10);
  LCD.clear();
  delay(10);
  LCD.print("Free Memory:");
  delay(10);
  LCD.selectLine(2);
  delay(10);
  LCD.print(String(freeMemory(),DEC) + " Bytes");
  
}*/

/*
void lcdLineOne(){  //puts the cursor at line 0 char 0.
   Serial.write(0xFE);   //command flag
   Serial.write(128);    //position
   delay(LCDdelay);
}
void lcdLineTwo(){  //puts the cursor at line 2 char 0.
   Serial.write(0xFE);   //command flag
   Serial.write(192);    //position
   delay(LCDdelay);
}*/



void lcdOn()
{
  LCD.display();
}
void lcdOff()
{
  LCD.noDisplay();
}

// wbp: goto with row & column
void lcdPosition(int row, int col) {
  LCD.write(0xFE);   //command flag
  LCD.write((col + row*64 + 128));    //position 
  delay(LCDdelay);
}
void lcdClear(){
  LCD.write(0xFE);   //command flag
  LCD.write(0x01);   //clear command.
  delay(LCDdelay);
}
void lcdBacklightOn() {  //turns on the backlight
  LCD.write(0x7C);   //command flag for backlight stuff
  LCD.write(157);    //light level.
  delay(LCDdelay);
}
void lcdBacklightOff(){  //turns off the backlight
  LCD.write(0x7C);   //command flag for backlight stuff
  LCD.write(128);     //light level for off.
   delay(LCDdelay);
}
void lcdSerCommand(){   //a general function to call the command flag for issuing all other commands   
  LCD.write(0xFE);

}



