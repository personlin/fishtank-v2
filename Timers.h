#ifndef _Timers_h
#define _Timers_h

void initTimers();
//void setHaveTime(boolean val);
boolean getHaveTime();
void setTimeFromTimestamp(unsigned long timestamp);

void setLightsOnTime(int totalMins);
void setLightsOffTime(int totalMins);
uint8_t getTimeOnHour();
uint8_t getTimeOnMinute();

uint8_t getTimeOffHour();
uint8_t getTimeOffMinute();

void setFeedTime(uint8_t totalMins);
uint8_t getAutoFeedHour();
uint8_t getAutoFeedMinute();
boolean getAutoFeedEnabled();
void setAutoFeedEnabled(boolean val);

#endif;